<?php
/**
 * The plugin bootstrap file
 *
 * @link              http://alibasheer.com
 * @since             1.0.0
 * @package           ZP_Forms
 *
 * @wordpress-plugin
 * Plugin Name:  ZeinPieces Forms
 * Plugin URI:   http://strategies-dc.com/
 * Description:  Provide the backend needed for ZeinPieces forms submission
 * Version:      1.0.0
 * Author:       Ali Basheer
 * Author URI:   http://alibasheer.com
 * License:      GPL-2.0+
 * License URI:  http://www.gnu.org/licenses/gpl-2.0.txt
 */

/**
 * If this file is called directly, abort.
 */
if (!defined('WPINC')) {
    die;
}
if (!class_exists('WP_List_Table')) {
    require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
}
include dirname(__FILE__) . '/inc/functions-ajax.php';
include dirname(__FILE__) . '/inc/functions-menus.php';
include dirname(__FILE__) . '/inc/functions-sql.php';
include dirname(__FILE__) . '/inc/functions-helpers.php';
include dirname(__FILE__) . '/inc/admin/contact-us-list-table.php';
include dirname(__FILE__) . '/inc/admin/newsletter-list-table.php';
//include dirname(__FILE__) . '/inc/admin/appointment-booking-list-table.php';
//include dirname(__FILE__) . '/inc/admin/franchise-request-list-table.php';
//include dirname(__FILE__) . '/inc/admin/survey-list-table.php';