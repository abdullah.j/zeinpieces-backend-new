<!DOCTYPE html>
<html>
<head>
    <title>Welcome to Zein Pieces.</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <style type="text/css">
        /* CLIENT-SPECIFIC STYLES */
        body, table, td, a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        /* Prevent WebKit and Windows mobile changing default text sizes */
        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        /* Remove spacing between tables in Outlook 2007 and up */
        img {
            -ms-interpolation-mode: bicubic;
        }

        /* Allow smoother rendering of resized image in Internet Explorer */

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /*a {*/
        /*color: inherit !important;*/
        /*text-decoration: none !important;*/
        /*font-size: inherit !important;*/
        /*font-family: inherit !important;*/
        /*font-weight: inherit !important;*/
        /*line-height: inherit !important;*/
        /*}*/

        a {
            text-decoration: none !important;
            /*color: #500778 !important;*/
            font-family: 'Trebuchet MS' !important;
        }

        .mobile-button {
            padding: 13px 12px 13px 12px !important;
        }


        /* MOBILE STYLES */
        @media screen and (max-width: 525px) {

            /* ALLOWS FOR FLUID TABLES */
            .wrapper {
                width: 100% !important;
                max-width: 100% !important;
            }

            /* ADJUSTS LAYOUT OF LOGO IMAGE */
            .logo img {
                margin: 0 auto !important;
            }

            .img-max {
                max-width: 100% !important;
                width: 100% !important;
                height: auto !important;
            }

            /* FULL-WIDTH TABLES */
            .responsive-table {
                width: 100% !important;
            }

            .no-mobile-padding {
                padding-right: 15px !important;
                padding-left: 15px !important;
            }

            .icon-width {
                max-width: 87px;
                margin-bottom: 16px;
            }

            .icon-td {
                padding-left: 20px;
            }


        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }

    </style>
</head>
<body style="margin: 0 !important; padding: 0 !important;">

<!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!--Logo-->
    <tr>
        <td align="center" bgcolor="#ffffff" style="padding: 0 15px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;" class="wrapper">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 0 15px 0;" class="logo" bgcolor="#fff">
                        <a href="http://zeinpieces.me/" target="_blank">
                            <img alt="Logo" src="https://zeinpieces.me/wp-content/uploads/2019/08/LOGO_ZP_Round.png"
                                 width="222" height="auto" style="display: block; width: 60px; height: auto;"
                                 border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!--Logo-->

    <!--FIRST LAYER-->
    <tr>
        <td align="center" bgcolor="#ffffff" style="padding: 30px 15px 10px 15px">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="max-width: 600px;" class="wrapper">
                <tr>
                    <td align="center"
                        style="padding-bottom:15px;font-size: 30px; line-height: 34px;color: #232323; font-weight: 300;">
                        Confirmed! You are now subscribed to our mailing list.
                    </td>
                </tr>
                <tr>
                    <td align="center"
                        style="padding-bottom: 15px;color: #232323; font-weight: 300;">
                        You may be wondering who we are. So, let us introduce ourselves properly.
                    </td>
                </tr>
                <tr>
                    <td align="center"
                        style="padding-bottom: 15px;color: #232323; font-weight: 300;">
                        We are a brand that crafts unconventional fine jewelry pieces that embrace meaning and beauty all at once. We aim to celebrate special moments with you — from milestones to love, romance, loyalty, friendship, and much more.
                    </td>
                </tr>
                <tr>
                    <td align="center"
                        style="padding-bottom: 15px;color: #232323; font-weight: 300;">
                        You are always at the center of our designs, our purpose, and our goals. You inspire us to create beautiful pieces of jewelry for beautiful people and moments.
                    </td>
                </tr>
                <tr>
                    <td align="center"
                        style="padding-bottom: 15px;color: #232323; font-weight: 700;">
                        Our Manifesto:
                    </td>
                </tr>
                <tr>
                    <td align="center"
                        style="padding-bottom: 15px;color: #232323;">
                        Life, in all corners of the world, relies on these three things: Design. Beauty. Meaning. Everything we see has been designed. Everything we encounter is beautiful in its own way. And everything we do should have meaning — and if it doesn't what is the point in life? In our passions, dreams, and goals? We sometimes get lost along the way. We sometimes lose confidence. We often wonder "what's the point" when met with challenges. But behind all the doubt and distress, there is a little voice in your head that keeps you going. That is the voice you should listen to. You only live once... and you should make it a life worth living.
                    </td>
                </tr>
                <tr>
                    <td align="center"
                        style="padding-bottom: 15px;color: #232323; font-weight: 300;">
                        Still don't have a personal account?
                    </td>
                </tr>
                <tr>
                    <td align="center"
                        style="padding-bottom: 15px;color: #232323; font-weight: 300;">
                        Register now for a chance to add items to your wishlist, save purchases, and enjoy the experience in a different way!
                    </td>
                </tr>
                <tr>
                    <td align="center"
                        style="padding-bottom: 15px;color: #232323; font-weight: 300;">
                        Feel like shopping?
                    </td>
                </tr>
                <tr>
                    <td align="center"
                        style="padding-bottom: 15px;color: #232323; font-weight: 300;">
                        You might want to check our NEW IN and Best Seller pieces.
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top" style="padding: 0; padding-bottom: 10px; padding-top: 10px; font-size: 14px; line-height: 16px; font-family: 'Trebuchet MS', sans-serif; color: #ffffff;" bgcolor="#ffffff">
                        <p style="padding:0;margin: 0;color:#232323;text-transform: uppercase;font-size: 12px;margin-bottom: 7px;">Follow us on social media</p>
                        <a href="https://www.facebook.com/zeinpieces" target="_blank"
                           style="display: inline-block; vertical-align: middle;">
                            <img alt="facebook icon" src="<?php echo plugin_dir_url(__FILE__); ?>img/facebook.png"
                                 width="20" height="20" style="display: inline-block; width: 36px;"
                                 border="0">
                        </a>&nbsp;
                        <a href="https://www.instagram.com/zeinpieces/" target="_blank"
                           style="display: inline-block; vertical-align: middle;">
                            <img alt="instagram icon" src="<?php echo plugin_dir_url(__FILE__); ?>img/Insta.png"
                                 width="20" height="20" style="display: inline-block; width: 36px;"
                                 border="0">
                        </a>&nbsp;

                    </td>
                </tr>

            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>

    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 0">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <!-- UNSUBSCRIBE COPY -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 600px;"
                   class="responsive-table">
                <tr>
                    <td align="center"
                        style="font-size: 11px; line-height: 13px; font-family: 'Trebuchet MS', sans-serif; color:#ffffff; padding: 10px 15px;" bgcolor="#000000">
                        ZEINPIECES 2021
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!--FOOTER-->
</table>
</body>
</html>
