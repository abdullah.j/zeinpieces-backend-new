<!DOCTYPE html>
<html>
<head>
    <title>New Customized Pieces Request</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <style type="text/css">
        /* CLIENT-SPECIFIC STYLES */
        body, table, td, a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        /* Prevent WebKit and Windows mobile changing default text sizes */
        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        /* Remove spacing between tables in Outlook 2007 and up */
        img {
            -ms-interpolation-mode: bicubic;
        }

        /* Allow smoother rendering of resized image in Internet Explorer */

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /*a {*/
        /*color: inherit !important;*/
        /*text-decoration: none !important;*/
        /*font-size: inherit !important;*/
        /*font-family: inherit !important;*/
        /*font-weight: inherit !important;*/
        /*line-height: inherit !important;*/
        /*}*/

        a {
            text-decoration: none !important;
            /*color: #500778 !important;*/
            font-family: 'Trebuchet MS' !important;
        }

        .mobile-button {
            padding: 13px 12px 13px 12px !important;
        }


        /* MOBILE STYLES */
        @media screen and (max-width: 525px) {

            /* ALLOWS FOR FLUID TABLES */
            .wrapper {
                width: 100% !important;
                max-width: 100% !important;
            }

            /* ADJUSTS LAYOUT OF LOGO IMAGE */
            .logo img {
                margin: 0 auto !important;
            }

            .img-max {
                max-width: 100% !important;
                width: 100% !important;
                height: auto !important;
            }

            /* FULL-WIDTH TABLES */
            .responsive-table {
                width: 100% !important;
            }

            .no-mobile-padding {
                padding-right: 15px !important;
                padding-left: 15px !important;
            }

            .icon-width {
                max-width: 87px;
                margin-bottom: 16px;
            }

            .icon-td {
                padding-left: 20px;
            }


        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }

    </style>
</head>
<body style="margin: 0 !important; padding: 0 !important;">

<!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!--Logo-->
    <tr>
        <td align="center" bgcolor="#ffffff" style="padding: 0 15px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;" class="wrapper">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 0 15px 0;" class="logo" bgcolor="#fcf1c4">
                        <a href="#" target="_blank">
                            <img alt="Logo" src="<?php echo plugin_dir_url(__FILE__); ?>img/logo.png"
                                 width="222" height="auto" style="display: block; width: 222px; height: auto;"
                                 border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!--Logo-->

    <!--FIRST LAYER-->
    <tr>
        <td align="center" bgcolor="#ffffff" style="padding: 30px 15px 30px 15px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="max-width: 600px;" class="wrapper">
                <tr>
                    <td align="center"
                        style="font-size: 28px; line-height: 32px; font-family: 'Trebuchet MS', sans-serif; color: #232323; text-transform: uppercase; font-weight: 700;">
                        new customized pieces request
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>

    <tr>
        <td align="center" bgcolor="#ffffff" style="padding: 0 15px 25px 15px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="max-width: 600px;" class="wrapper">
                <tr>
                    <td align="left" style="font-size: 16px; line-height: 22px; font-family: 'Trebuchet MS', sans-serif; color: #232323; padding-bottom: 20px; padding-left: 15px; padding-right: 15px;">
                        Dear admin,
                    </td>
                </tr>

                <tr>
                    <td align="left" style="font-size: 16px; line-height: 22px; font-family: 'Trebuchet MS', sans-serif; color: #232323; padding-bottom: 20px; padding-left: 15px; padding-right: 15px;">
                        This is to inform you that you received a new request for a customized pieces at <a href="<?php echo site_url();?>" target="_blank" style="font-size: 16px; line-height: 22px; font-family: 'Trebuchet MS', sans-serif !important; color: #ffcc00 !important; text-decoration: none !important;">Zein Pieces</a> with the below details:
                    </td>
                </tr>

                <tr>
                    <td align="left" style="font-size: 16px; line-height: 22px; font-family: 'Trebuchet MS', sans-serif; color: #232323; padding-left: 15px; padding-right: 15px;">
                        <strong>Full name:</strong> <?php echo $params['first-name']." ".$params['last-name']; ?>
                    </td>
                </tr>

                <tr>
                    <td align="left" style="font-size: 16px; line-height: 22px; font-family: 'Trebuchet MS', sans-serif; color: #232323; padding-left: 15px; padding-right: 15px;">
                        <strong>Subject:</strong> <?php echo $params['subject']; ?>
                    </td>
                </tr>

                <tr>
                    <td align="left" style="font-size: 16px; line-height: 22px; font-family: 'Trebuchet MS', sans-serif; color: #232323; padding-left: 15px; padding-right: 15px;">
                        <strong>Email address:</strong> <?php echo $params['email-address']; ?>
                    </td>
                </tr>

                <tr>
                    <td align="left" style="font-size: 16px; line-height: 22px; font-family: 'Trebuchet MS', sans-serif; color: #232323; padding-left: 15px; padding-right: 15px;">
                        <strong>Mobile number:</strong> <?php echo $params['telephone']; ?>
                    </td>
                </tr>

                <tr>
                    <td align="left" style="font-size: 16px; line-height: 22px; font-family: 'Trebuchet MS', sans-serif; color: #232323; padding-left: 15px; padding-right: 15px;">
                        <strong>Country:</strong> <?php echo $params['country']; ?>
                    </td>
                </tr>

                <tr>
                    <td align="left" style="font-size: 16px; line-height: 22px; font-family: 'Trebuchet MS', sans-serif; color: #232323; padding-bottom: 20px; padding-left: 15px; padding-right: 15px;">
                        <strong>Message:</strong> <?php echo $params['message']; ?>
                    </td>
                </tr>
                <?php

                if(!empty($_FILES["attachments"]["name"])){
                    $file_file = $_FILES['attachments'];
                    $imageName = $file_file['name'];
                    $upload_dir = wp_upload_dir();
                    //$imagepath = $upload_dir['baseurl']."/".$imageName;

                ?>

                <tr>
                    <td align="left" style="font-size: 16px; line-height: 22px; font-family: 'Trebuchet MS', sans-serif; color: #232323; padding-bottom: 20px; padding-left: 15px; padding-right: 15px;">
                        <strong>File:</strong> <?php echo $_SESSION['personalizeImagePath']; ?>
                    </td>
                </tr>
            <?php } ?>
                <tr>
                    <td align="left" style="font-size: 16px; line-height: 22px; font-family: 'Trebuchet MS', sans-serif; color: #232323; padding-left: 15px; padding-right: 15px;">
                        Kindly review and contact the user for confirmation.
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!--FIRST LAYER-->

    <!--FOOTER-->
    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 0 15px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;" class="wrapper">
                <tr>
                    <td align="center" valign="top" style="padding: 0; padding-bottom: 5px; padding-top: 20px; font-size: 14px; line-height: 16px; font-family: 'Trebuchet MS', sans-serif; color: #ffffff;" bgcolor="#d1d3d3">
                        <a href="https://www.facebook.com/zeinpieces" target="_blank"
                           style="display: inline-block; vertical-align: middle;">
                            <img alt="facebook icon" src="<?php echo plugin_dir_url(__FILE__); ?>img/facebook-ico.png"
                                 width="20" height="20" style="display: inline-block; width: 20px; height: 20px;"
                                 border="0">
                        </a>&nbsp;
                        <a href="https://www.instagram.com/zeinpieces/" target="_blank"
                           style="display: inline-block; vertical-align: middle;">
                            <img alt="instagram icon" src="<?php echo plugin_dir_url(__FILE__); ?>img/instagram-ico.png"
                                 width="20" height="20" style="display: inline-block; width: 20px; height: 20px;"
                                 border="0">
                        </a>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="padding: 0;font-size: 15px; line-height: 17px; font-family: 'Trebuchet MS', sans-serif; color: #f8d132 !important; text-decoration: none !important; padding-bottom: 15px;" bgcolor="#d1d3d3">
                        <a href="<?php echo site_url();?>" target="_blank"
                           style="display: inline-block; font-size: 15px; line-height: 17px; font-family: 'Trebuchet MS', sans-serif; color: #ffffff !important; text-decoration: none !important;">
                            zeinpieces.me
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>

    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 0">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <!-- UNSUBSCRIBE COPY -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 600px;"
                   class="responsive-table">
                <tr>
                    <td align="center"
                        style="font-size: 11px; line-height: 13px; font-family: 'Trebuchet MS', sans-serif; color:#ffffff; padding: 10px 15px;" bgcolor="#000000">
                        &copy; 2019 Zeinpieces
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!--FOOTER-->
</table>
</body>
</html>
