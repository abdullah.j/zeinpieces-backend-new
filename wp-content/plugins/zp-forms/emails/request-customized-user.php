<!DOCTYPE html>
<html>
<head>
    <title>Customized Pieces Request Received</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <style type="text/css">
        /* CLIENT-SPECIFIC STYLES */
        body, table, td, a {
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        /* Prevent WebKit and Windows mobile changing default text sizes */
        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        /* Remove spacing between tables in Outlook 2007 and up */
        img {
            -ms-interpolation-mode: bicubic;
        }

        /* Allow smoother rendering of resized image in Internet Explorer */

        /* RESET STYLES */
        img {
            border: 0;
            height: auto;
            line-height: 100%;
            outline: none;
            text-decoration: none;
        }

        table {
            border-collapse: collapse !important;
        }

        body {
            height: 100% !important;
            margin: 0 !important;
            padding: 0 !important;
            width: 100% !important;
        }

        /* iOS BLUE LINKS */
        a[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
            font-size: inherit !important;
            font-family: inherit !important;
            font-weight: inherit !important;
            line-height: inherit !important;
        }

        /*a {*/
        /*color: inherit !important;*/
        /*text-decoration: none !important;*/
        /*font-size: inherit !important;*/
        /*font-family: inherit !important;*/
        /*font-weight: inherit !important;*/
        /*line-height: inherit !important;*/
        /*}*/

        a {
            text-decoration: none !important;
            /*color: #500778 !important;*/
            font-family: 'Trebuchet MS' !important;
        }

        .mobile-button {
            padding: 13px 12px 13px 12px !important;
        }


        /* MOBILE STYLES */
        @media screen and (max-width: 525px) {

            /* ALLOWS FOR FLUID TABLES */
            .wrapper {
                width: 100% !important;
                max-width: 100% !important;
            }

            /* ADJUSTS LAYOUT OF LOGO IMAGE */
            .logo img {
                margin: 0 auto !important;
            }

            .img-max {
                max-width: 100% !important;
                width: 100% !important;
                height: auto !important;
            }

            /* FULL-WIDTH TABLES */
            .responsive-table {
                width: 100% !important;
            }

            .no-mobile-padding {
                padding-right: 15px !important;
                padding-left: 15px !important;
            }

            .icon-width {
                max-width: 87px;
                margin-bottom: 16px;
            }

            .icon-td {
                padding-left: 20px;
            }


        }

        /* ANDROID CENTER FIX */
        div[style*="margin: 16px 0;"] {
            margin: 0 !important;
        }

    </style>
</head>
<body style="margin: 0 !important; padding: 0 !important;">

<!-- HEADER -->
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <!--Logo-->
    <tr>
        <td align="center" bgcolor="#ffffff" style="padding: 0 15px;">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;" class="wrapper">
                <tr>
                    <td align="center" valign="top" style="padding: 40px 0 15px 0;" class="logo" bgcolor="#fff">
                        <a href="http://zeinpieces.me/" target="_blank">
                            <img alt="Logo" src="https://zeinpieces.me/wp-content/uploads/2019/08/LOGO_ZP_Round.png"
                                 width="222" height="auto" style="display: block; width: 60px; height: auto;"
                                 border="0">
                        </a>
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!--Logo-->

    <!--FIRST LAYER-->
    <tr>
        <td align="center" bgcolor="#ffffff" style="padding: 30px 15px 10px 15px">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <table cellpadding="0" cellspacing="0" border="0" width="100%" style="max-width: 600px;" class="wrapper">
                <tr>
                    <td align="center"
                        style="font-size: 38px; line-height: 38px; font-family: 'PlayfairDisplay'; color: #232323; text-transform: uppercase; font-weight: 300;">
                        thank you for <br>
                        choosing <a href="<?php echo site_url();?>" style="color:#232323;font-family: 'PlayfairDisplay' !important;";>zeinpieces.me</a>
                    </td>
                </tr>

                <tr>
                    <td align="center"
                        style="font-size: 16px; font-family: 'Trebuchet MS', sans-serif; color: #232323; font-weight: 300;">

                        <p>Dear Ms. <?php echo $params['last-name']; ?>,<br>
                            Your request has been successfully submitted.<br>
                        We will review it and contact you shortly.</p>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top" style="padding: 0; padding-bottom: 10px; padding-top: 10px; font-size: 14px; line-height: 16px; font-family: 'Trebuchet MS', sans-serif; color: #ffffff;" bgcolor="#ffffff">
                        <p style="padding:0;margin: 0;color:#232323;text-transform: uppercase;font-size: 12px;margin-bottom: 7px;">Connect with us</p>
                        <a href="https://www.facebook.com/zeinpieces" target="_blank"
                           style="display: inline-block; vertical-align: middle;">
                            <img alt="facebook icon" src="<?php echo plugin_dir_url(__FILE__); ?>img/facebook.png"
                                 width="20" height="20" style="display: inline-block; width: 36px;"
                                 border="0">
                        </a>&nbsp;
                        <a href="https://www.instagram.com/zeinpieces/" target="_blank"
                           style="display: inline-block; vertical-align: middle;">
                            <img alt="instagram icon" src="<?php echo plugin_dir_url(__FILE__); ?>img/Insta.png"
                                 width="20" height="20" style="display: inline-block; width: 36px;"
                                 border="0">
                        </a>&nbsp;

                    </td>
                </tr>

            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>

    <tr>
        <td bgcolor="#ffffff" align="center" style="padding: 0">
            <!--[if (gte mso 9)|(IE)]>
            <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                    <td align="center" valign="top" width="600">
            <![endif]-->
            <!-- UNSUBSCRIBE COPY -->
            <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width: 600px;"
                   class="responsive-table">
                <tr>
                    <td align="center"
                        style="font-size: 11px; line-height: 13px; font-family: 'Trebuchet MS', sans-serif; color:#ffffff; padding: 10px 15px;" bgcolor="#000000">
                         ZEINPIECES 2019
                    </td>
                </tr>
            </table>
            <!--[if (gte mso 9)|(IE)]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </td>
    </tr>
    <!--FOOTER-->
</table>
</body>
</html>
