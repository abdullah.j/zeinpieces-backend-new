<?php
/**
 * Class for displaying Contact Us emails
 */
class Contact_Us_List_Table extends WP_List_Table {
    public function get_columns() {
        $table_columns = array(
            'date'	  => __( 'Registered On', 'zeinpieces' ),
            'name'    => __( 'Name', 'zeinpieces' ),
            'email'   => __( 'Email', 'zeinpieces' ),
            'number'  => __( 'Number', 'zeinpieces' ),
            'subject' => __( 'Subject', 'zeinpieces' ),
            'message' => __( 'Message', 'zeinpieces' ),
            'type' => __( 'Type', 'zeinpieces' ),
        );
        return $table_columns;
    }
    public function no_items() {
        _e( 'No requests avaliable.', 'zeinpieces' );
    }
    public function prepare_items() {
        global $wpdb;
        $per_page = 200;
        $columns  = $this->get_columns();
        $hidden   = array();
        $this->_column_headers = array( $columns, $hidden );
        $table_contact_us = $wpdb->prefix . 'zp_contact_us';
        $results = $wpdb->get_results("SELECT * FROM $table_contact_us order by id desc");
        $data = array();
        foreach ( $results as $item ) {
            $row['date']    = date( 'Y-m-d', strtotime( $item->date ) ) ;
            $row['email']   = $item->email;
            $row['name']    = $item->name;
            $row['number']  = $item->number;
            $row['subject'] = $item->subject;
            $row['message'] = $item->message;
            $row['type'] = $item->type;
            $data[] = $row;
        }
        $current_page = $this->get_pagenum();
        $total_items = count( $data );
        $data = array_slice( $data, ( ( $current_page - 1 ) * $per_page ), $per_page );
        $this->items = $data;
        $this->set_pagination_args( array(
            'total_items' => $total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil( $total_items / $per_page ),
        ) );
    }
    public function column_default( $item, $column_name ) {
        switch ( $column_name ) {
            default:
                return $item[$column_name];
        }
    }
}