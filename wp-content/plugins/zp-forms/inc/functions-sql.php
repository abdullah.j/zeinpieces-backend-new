<?php
function zp_forms_db_tables() {

    // change the version to update roles
    $zp_forms_db_version = "1.1";

    if ( $zp_forms_db_version != get_option( "zp_forms_db_version" ) ) {
        global $wpdb;

        $table_contact_us = $wpdb->prefix . 'zp_contact_us';
        $sql1 = "CREATE TABLE $table_contact_us (
			`id` INTEGER (10) NOT NULL AUTO_INCREMENT,
			`date` DATETIME NOT NULL,
			name VARCHAR (100) NOT NULL,
			email VARCHAR (100) NOT NULL,
			subject VARCHAR (250) NOT NULL,
			number VARCHAR (50) NOT NULL,
			message TEXT NOT NULL,
			PRIMARY KEY (`id`))";

        $table_newsletter = $wpdb->prefix . 'zp_newsletter';
        $sql2 = "CREATE TABLE $table_newsletter (
			`id` INTEGER (10) NOT NULL AUTO_INCREMENT,
			`date` DATETIME NOT NULL,
			email VARCHAR (100) NOT NULL,
			PRIMARY KEY (`id`,email),
			UNIQUE (email))";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql1);
        dbDelta($sql2);

        update_option('zp_forms_db_version', $zp_forms_db_version);
    }
}
add_action('after_setup_theme', 'zp_forms_db_tables');