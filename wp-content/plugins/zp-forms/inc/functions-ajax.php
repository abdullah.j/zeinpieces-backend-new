<?php

function zp_contact_us() {

    if ( isset( $_POST["email"] ) && wp_verify_nonce($_POST['zp_form_nonce'], 'zp-nonce')) {

        if(isset($_POST['g-recaptcha-response'])) {
            $captcha=$_POST['g-recaptcha-response'];
        }
        if(!$captcha){
            wp_send_json(array( 'nocaptcha' => 'true'));
            exit;
          }
        // calling google recaptcha api.
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LeDcqQUAAAAAD46yEbUPtQYTqC5-c_e2gx0h_xZ&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);

        if($response.success==false) {
            wp_send_json(array( 'spam' => 'true'));
        }else {
            global $wpdb;
            $table_contact_us = $wpdb->prefix . 'zp_contact_us';

            $contact_us = $wpdb->insert(
                $table_contact_us,
                array('date' => date('Y-m-d H:i:s'),
                    'name' => $_POST["name"],
                    'email' => $_POST["email"],
                    'number' => $_POST["number"],
                    'message' => $_POST["message"],
                    'subject' => $_POST["subject"]
                )
            );

            if (!$contact_us) {
                wp_send_json(array('error' => array('message' => 'Sorry, there is an error submitting contact us message!')));
            }

            // send email
            if ($contact_us && !is_wp_error($contact_us)) {

                $user_content = getRenderedHTML(WP_PLUGIN_DIR . '/zp-forms/emails/contact-user.php', $_POST);
                wp_mail($_POST["email"], 'Message Received | ZeinPieces', $user_content);

                //$admin_content = getRenderedHTML(WP_PLUGIN_DIR . '/zp-forms/emails/contact-admin.php', $_POST);
                //wp_mail('info@zeinpieces.me', 'Contact us Request | ZeinPieces', $admin_content);
            }
            wp_send_json(array('success' => array('message' => 'Thank you for contacting us')));

           }

    } else {
        wp_send_json(array( 'error' => array( 'message' => 'Sorry, there is an error in the submitted fields!')));
    }
}
add_action('wp_ajax_nopriv_zp_contact_us', 'zp_contact_us');
add_action('wp_ajax_zp_contact_us', 'zp_contact_us');

function zp_newsletter() {

    if ( isset( $_POST["email"] ) && wp_verify_nonce($_POST['zp_form_nonce'], 'zp-nonce')) {

        if(isset($_POST['g-recaptcha-response'])) {
            $captcha=$_POST['g-recaptcha-response'];
        }
        if(!$captcha){
            wp_send_json(array( 'nocaptcha' => 'true'));
            exit;
        }
        // calling google recaptcha api.
        $response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LeDcqQUAAAAAD46yEbUPtQYTqC5-c_e2gx0h_xZ&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);

        if($response.success==false) {
            wp_send_json(array( 'spam' => 'true'));
        }else {

            global $wpdb;
            $table_newsletter = $wpdb->prefix . 'zp_newsletter';

            $newsletter = $wpdb->insert(
                $table_newsletter,
                array('date' => date('Y-m-d H:i:s'),
                    'email' => $_POST["email"],
                )
            );

            if (!$newsletter) {
                wp_send_json(array('error' => array('message' => 'Sorry, there is an error submitting contact us message!')));
            }

            // send email
            if ($newsletter && !is_wp_error($newsletter)) {

                $user_content = getRenderedHTML(WP_PLUGIN_DIR . '/zp-forms/emails/newsletter-user.php', $_POST);
                wp_mail($_POST["email"], 'Newsletter Subscription | ZeinPieces', $user_content);

                $admin_content = getRenderedHTML(WP_PLUGIN_DIR . '/zp-forms/emails/newsletter-admin.php', $_POST);
                wp_mail('info@zeinpieces.me', 'Newsletter Subscription | ZeinPieces', $admin_content);
            }
            wp_send_json(array('success' => array('message' => 'Thank you for your subscription')));
        }
    } else {
        wp_send_json(array( 'error' => array( 'message' => 'Sorry, there is an error in the submitted fields!')));
    }

}
add_action('wp_ajax_nopriv_zp_newsletter', 'zp_newsletter');
add_action('wp_ajax_zp_newsletter', 'zp_newsletter');

function zp_download_csv() {
    global $wpdb;
    $table_name = ( isset($_GET['table']) ) ? $_GET['table'] : 0;
    if( $table_name === 0 ) {
        wp_send_json("not allowed");
    }
    $table = $wpdb->prefix . $table_name;
    $results = $wpdb->get_results("SELECT * FROM $table");
    if( $results !== false ):
        $now = new DateTime();
        $fn1 = $table_name . '-export-list-date-'. $now->format('YmdHis'). '.csv';
        $fn2 = WP_PLUGIN_DIR .'/zp-forms/exports/' . $fn1;
        $fp = fopen($fn2, 'w');
        foreach( $results as $key => $row ) {
            if ($key == 0) {
                fputcsv($fp, array_keys((array)$row));
            }
            fputcsv($fp, (array)$row);
        }
        $fp = fopen($fn2, 'r');
        $fc = fread($fp, filesize($fn2) );
        fclose($fp);
        header('Content-Encoding: UTF-8');
        header('Content-type: text/csv; charset=UTF-8');
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename=' . $fn1);
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        echo "\xEF\xBB\xBF"; // UTF-8 BOM
        echo $fc;
        exit;
    endif;
    return false;
}
add_action('wp_ajax_download_csv', 'zp_download_csv');