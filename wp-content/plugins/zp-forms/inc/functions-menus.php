<?php

function forms_add_menu_items() {

    add_menu_page('Forms List','Forms List', 'activate_plugins', 'forms-list', 'contact_us_list_page', 'dashicons-media-spreadsheet' );

    add_submenu_page(
        'forms-list',   //or 'options.php'
        'Contact Us',
        'Contact Us',
        'edit_pages',
        'forms-list',
        'contact_us_list_page'
    );

    add_submenu_page(
        'forms-list',   //or 'options.php'
        'Newsletter Subscribers',
        'Newsletter',
        'edit_pages',
        'newsletter-subscribers',
        'newsletter_subscribers_list_page'
    );

}
add_action('admin_menu', 'forms_add_menu_items');

function contact_us_list_page() {
    $contactus_table = new Contact_Us_List_Table();
    $contactus_table->prepare_items();
    include dirname(__FILE__) . '/views/contact-us.php';
}

function newsletter_subscribers_list_page() {
    $newsletter_table = new Newsletter_Subscribers_List_Table();
    $newsletter_table->prepare_items();
    include dirname(__FILE__) . '/views/newsletter.php';
}