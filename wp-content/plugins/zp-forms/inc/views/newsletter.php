<?php
/**
 * Newsletter admin page view
 */
?>
<div class="wrap">
    <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
    <form id="" method="get">
        <?php $newsletter_table->display(); ?>
    </form>
    <p><a href="<?php echo admin_url( 'admin-ajax.php' ); ?>?action=download_csv&table=zp_newsletter" class="button button-primary">Export</a></p>
</div>