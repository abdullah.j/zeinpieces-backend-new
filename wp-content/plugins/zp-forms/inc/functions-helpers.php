<?php
function elixir_set_content_type() {
    return "text/html";
}
add_filter( 'wp_mail_content_type','elixir_set_content_type' );

function getRenderedHTML($path, $params = null) {

    ob_start();
    include( $path );
    $var = ob_get_contents();
    ob_end_clean();

    return $var;
}