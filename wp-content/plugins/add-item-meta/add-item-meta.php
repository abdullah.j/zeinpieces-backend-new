<?php
/*
Plugin Name: Add Item Meta & Order Meta for your WooComerce order
Description: A simple demo plugin on how to add add cart item meta & order item meta for your WooComerce order.
Author: WICK
Version: 1.0
*/

session_start();
add_action( 'woocommerce_before_add_to_cart_button', 'add_fields_before_add_to_cart' );
function add_fields_before_add_to_cart( ) {
    global $product;

    $terms = get_the_terms ( $product_id, 'product_cat' );

    foreach ( $terms as $term ) $categories_ids[] = $term->term_id;

    //print_r($categories_ids);
    $cat_id = $terms[0]->term_id;
    $cat = get_field('sizes', 'product_cat_'.$cat_id);
    ?>
    <table>
        <?php if($cat_id!='67'){?>
            <tr>
                <td>
                    <?php _e( "Piece Color:", "aoim"); ?>
                </td>
                <td>
                    <select id="cfc_pa_color" class="selectpicker" name="cfc_pa_color" data-attribute_name="attribute_pa_color" data-show_option_none="yes" tabindex="-98">
                        <option value="Default" class="attached enabled">Default</option>
                        <option value="Yellow Gold" class="attached enabled">Yellow Gold</option>
                        <option value="Rose Gold" class="attached enabled">Rose Gold</option>
                        <option value="White Gold" class="attached enabled">White Gold</option>
                    </select>

                </td>

            </tr>

            <?php if($cat_id=='16' || $cat_id=='18' || $cat_id=='71' || $cat_id=='77' || $cat_id=='78' || $cat_id=='79'){?>
                <tr><td><br></td></tr>
                <tr>
                    <td>
                        <?php _e( "Chain Color:", "aoim"); ?>
                    </td>
                    <td>
                        <select id="chain_pa_color" class="selectpicker" name="chain_pa_color" data-attribute_name="attribute_pa_color" data-show_option_none="yes" tabindex="-98">
                            <option value="Default" class="attached enabled">Default</option>
                            <option value="Yellow Gold" class="attached enabled">Yellow Gold</option>
                            <option value="Rose Gold" class="attached enabled">Rose Gold</option>
                            <option value="White Gold" class="attached enabled">White Gold</option>
                        </select>

                    </td>

                </tr>


            <?php } ?>

            <?php if ( in_array( 72, $categories_ids ) ) { ?>
                <tr><td><br></td></tr>
                <tr>
                    <td>
                        <?php _e( "Leather Color:", "aoim"); ?>
                    </td>
                    <td style="position: relative" class="leather">
                        <div class="dropdown picker">
                            <input style="width: 100%;cursor: pointer" type="text" value="Default" id="pick_color" name="leather_color" readonly>

                        </div>

                        <div class="zp-color-picker" style="display: none">
                            <?php
                            // WP_Query arguments
                            $args = array (
                                'post_type'              => array( 'color' ),
                                'post_status'            => array( 'publish' ),
                                'nopaging'               => true,
                                'order'                  => 'ASC',
                                'orderby'                => 'menu_order',
                                'cat'                    => 76
                            );

                            // The Query
                            $colors = new WP_Query( $args );

                            // The Loop
                            if ( $colors->have_posts() ) {
                                while ( $colors->have_posts() ) {
                                    $colors->the_post();

                                    $bgcolor = get_field('color' , get_the_ID());
                                    ?>
                                    <div class="color-icons" data-style-color="<?php the_title(); ?>">
                                        <span><img src="<?php the_field('icon'); ?>"></span>
                                        <div><?php the_title(); ?></div>
                                    </div>
                                <?php }
                            }

                            // Restore original Post Data
                            wp_reset_postdata();
                            ?>


                        </div>

                    </td>

                </tr>


            <?php } ?>


            <?php if ( in_array( 73, $categories_ids ) ) {?>
                <tr><td><br></td></tr>
                <tr>
                    <td>
                        <?php _e( "Rope Color:", "aoim"); ?>
                    </td>
                    <td style="position: relative" class="rope">

                        <div class="dropdown picker">

                            <input style="width: 100%;cursor: pointer" autocomplete="off" type="text" placeholder="Default" value="" id="pick_color" name="rope_color">
                            <input type="hidden" name="ropebg" value="" id="ropeid">
                        </div>


                        <div class="zp-color-picker" style="display: none">
                            <?php
                            // WP_Query arguments
                            $args = array (
                                'post_type'              => array( 'color' ),
                                'post_status'            => array( 'publish' ),
                                'category__not_in'       => 76 ,
                                'nopaging'               => true,
                                'order'                  => 'ASC',
                                'orderby'                => 'menu_order',
                            );

                            // The Query
                            $colors = new WP_Query( $args );

                            // The Loop
                            if ( $colors->have_posts() ) {
                                while ( $colors->have_posts() ) {
                                    $colors->the_post();

                                    $bgcolor = get_field('color' , get_the_ID());
                                    ?>
                                    <div class="color-icons" data-style-color="<?php the_title(); ?>" data-bg="<?php echo $bgcolor; ?>">
                                        <span style="background-color: <?php echo $bgcolor; ?>; <?php if($bgcolor=='#ffffff')echo 'border:1px solid #e3e3e3e3'; ?>"></span>
                                        <div><?php the_title(); ?></div>
                                    </div>
                                <?php }
                            }

                            // Restore original Post Data
                            wp_reset_postdata();
                            ?>


                        </div>

                    </td>

                </tr>


            <?php } ?>


            <?php if ( in_array( 19, $categories_ids ) || in_array( 78, $categories_ids )) { ?>
                <tr><td><br></td></tr>
                <tr>
                    <td>
                        <?php _e( "Chain Size:", "aoim"); ?>
                    </td>

                    <td style="position: relative">

                        <div class="dropdown picker">
                            <input style="width: 100%;cursor: pointer" autocomplete="off" type="text" placeholder="Default" value="" id="pick_size" name="pick_size" required>
                        </div>


                        <div class="zp-size-picker" style="display: none">
                            <?php

                            ?>
                            <div class="size-icons" data-style-size="40">
                                <div>40</div>
                            </div>

                            <div class="size-icons" data-style-size="41">
                                <div>41</div>
                            </div>

                            <div class="size-icons" data-style-size="42">
                                <div>42</div>
                            </div>

                            <div class="size-icons" data-style-size="43">
                                <div>43</div>
                            </div>

                            <div class="size-icons" data-style-size="44">
                                <div>44</div>
                            </div>

                            <div class="size-icons" data-style-size="45">
                                <div>45</div>
                            </div>

                            <div class="size-icons" data-style-size="46">
                                <div>46</div>
                            </div>

                            <div class="size-icons" data-style-size="47">
                                <div>47</div>
                            </div>

                            <div class="size-icons" data-style-size="48">
                                <div>48</div>
                            </div>

                            <div class="size-icons" data-style-size="49">
                                <div>49</div>
                            </div>

                            <div class="size-icons" data-style-size="50">
                                <div>50</div>
                            </div>

                            <div class="size-icons" data-style-size="51">
                                <div>51</div>
                            </div>

                            <div class="size-icons" data-style-size="52">
                                <div>52</div>
                            </div>

                            <div class="size-icons" data-style-size="53">
                                <div>53</div>
                            </div>

                            <div class="size-icons" data-style-size="54">
                                <div>54</div>
                            </div>

                            <div class="size-icons" data-style-size="55">
                                <div>55</div>
                            </div>

                            <div class="size-icons" data-style-size="56">
                                <div>56</div>
                            </div>

                            <div class="size-icons" data-style-size="57">
                                <div>57</div>
                            </div>


                            <div class="size-icons" data-style-size="58">
                                <div>58</div>
                            </div>

                            <div class="size-icons" data-style-size="59">
                                <div>59</div>
                            </div>

                            <div class="size-icons" data-style-size="60">
                                <div>60</div>
                            </div>

                            <div class="size-icons" data-style-size="61">
                                <div>61</div>
                            </div>

                            <div class="size-icons" data-style-size="62">
                                <div>62</div>
                            </div>

                            <div class="size-icons" data-style-size="63">
                                <div>63</div>
                            </div>


                            <div class="size-icons" data-style-size="64">
                                <div>64</div>
                            </div>

                            <div class="size-icons" data-style-size="65">
                                <div>65</div>
                            </div>


                            <div class="size-icons" data-style-size="66">
                                <div>66</div>
                            </div>

                            <div class="size-icons" data-style-size="67">
                                <div>67</div>
                            </div>

                        </div>

                    </td>

                    <td align="right"><img style="cursor: pointer;width: 20px;" data-toggle="modal" data-target="#ringModal" src="<?php echo THEME_URL; ?>/assets/img/what.jpg"></td>

                </tr>


            <?php } ?>


            <?php if ( in_array( 16, $categories_ids )) { ?>
                <tr><td><br></td></tr>
                <tr>
                    <td>
                        <?php _e( "Chain Size:", "aoim"); ?>
                    </td>

                    <td style="position: relative" class="necklace-picker">

                        <div class="dropdown picker">
                            <input style="width: 100%;cursor: pointer" autocomplete="off" type="text" placeholder="Default" value="" id="pick_size" name="pick_size" required>
                        </div>


                        <div class="zp-size-picker" style="display: none">
                            <div class="size-icons" data-style-size="35">
                                <div>35</div>
                            </div>

                            <div class="size-icons" data-style-size="40">
                                <div>40</div>
                            </div>

                            <div class="size-icons" data-style-size="46">
                                <div>46</div>
                            </div>

                            <div class="size-icons" data-style-size="50">
                                <div>50</div>
                            </div>

                            <div class="size-icons" data-style-size="60">
                                <div>60</div>
                            </div>

                            <div class="size-icons" data-style-size="76">
                                <div>76</div>
                            </div>

                            <div class="size-icons" data-style-size="84">
                                <div>84</div>
                            </div>


                        </div>

                    </td>

                    <td align="right"><img style="cursor: pointer;width: 20px;" data-toggle="modal" data-target="#necklacesModal" src="<?php echo THEME_URL; ?>/assets/img/what.jpg"></td>

                </tr>


            <?php } ?>



            <?php if ( in_array( 18, $categories_ids ) ||  in_array( 52, $categories_ids ) ||  in_array( 71, $categories_ids )) { ?>

                <tr><td><br></td></tr>
                <tr>
                    <td>
                        <?php _e( "Chain Size:", "aoim"); ?>

                    </td>
                    <td>
                        <input style="width: 100%" autocomplete="off" type="text" placeholder="" value="" name="pick_size" required>
                    </td>

                    <td align="right">
                        <?php if ( in_array( 71, $categories_ids )){?>
                        <img style="cursor: pointer;width: 20px;" data-toggle="modal" data-target="#ankletModal" src="<?php echo THEME_URL; ?>/assets/img/what.jpg">
                        <?php }elseif( in_array( 52, $categories_ids )) { ?>
                            <img style="cursor: pointer;width: 20px;" data-toggle="modal" data-target="#bangleModal" src="<?php echo THEME_URL; ?>/assets/img/what.jpg">
                            <?php } else{ ?>
                            <img style="cursor: pointer;width: 20px;" data-toggle="modal" data-target="#braceletsModal" src="<?php echo THEME_URL; ?>/assets/img/what.jpg">
                        <?php } ?>
                    </td>
                </tr>


            <?php } ?>

            <?php if ( in_array( 70, $categories_ids ) ||  in_array( 73, $categories_ids )) { ?>

                <tr><td><br></td></tr>
                <tr>
                    <td>
                        <?php _e( "Chain Size:", "aoim"); ?>
                    </td>
                    <td>
                        <select id="cat_size_pa_color" class="selectpicker" name="cat_size_pa_color" data-attribute_name="attribute_pa_color" data-show_option_none="yes" tabindex="-98">
                            <option value="Default" class="attached enabled">Default</option>
                            <option value="Yellow Gold" class="attached enabled">Baby Size</option>
                            <option value="Women Size" class="attached enabled">Women Size</option>
                            <option value="Men Size" class="attached enabled">Men Size</option>
                        </select>

                    </td>

                </tr>


            <?php } ?>

            <?php

            $terms = get_the_terms ( $product_id, 'product_cat' );
            $cat_id = $terms[0]->term_id;
            $cat = get_field('sizes', 'product_cat_'.$cat_id);
            if($cat!=''){
                ?>

                <tr><td><br></td></tr>


                <tr>
                    <td>
                        <?php _e( "Chain Size:", "aoim"); ?>
                    </td>
                    <td>
                        <select id="cfc_pa_size" class="selectpicker" name="cfc_pa_size" data-attribute_name="attribute_pa_size-ring" data-show_option_none="yes" tabindex="-98">
                            <?php


                            $pieces = explode(",", $cat);

                            foreach($pieces as $category) :
                                echo '<option value="'.$category.'" class="attached enabled">';
                                echo $category;
                                echo '</option>';
                            endforeach;
                            ?>
                        </select>

                    </td>
                </tr>
            <?php } ?>

            <tr><td><br></td></tr>
        <?php } ?>
        <?php if($cat_id=='67'){?>
            <tr>
                <td>
                    <label><?php _e( "AMOUNT (USD) ", "aoim"); ?></label><br>
                    <input style="width:75%" name="client_price" class="input-text " id="client_price" required><br><br>

                </td>

            </tr>

            <tr><td><br></td></tr>
            <tr>
                <td>
                    <label><?php _e( "YOUR NAME ", "aoim"); ?></label><br>
                    <input style="width:75%" name="client_name" class="input-text " id="client_name" required><br><br>

                </td>

            </tr>

            <tr><td><br></td></tr>

            <tr>
            <td>
                <label><?php _e( "Order details ", "aoim"); ?></label><br>
                <textarea style="width:75%" name="client_message" class="input-text " id="client_message" placeholder="Order details..." rows="5" cols="5"></textarea>

            </td>

            </tr><?php } ?>
    </table>
    <?php
}
/**
 * Add data to cart item
 */
add_filter( 'woocommerce_add_cart_item_data', 'add_cart_item_data', 25, 2 );
function add_cart_item_data( $cart_item_meta, $product_id ) {
    if ( isset( $_POST ['cfc_pa_color'] ) || isset( $_POST ['cfc_pa_size'] ) || isset( $_POST ['leather_color'] ) || isset( $_POST ['rope_color'] ) || isset( $_POST ['client_name'] ) || isset( $_POST ['client_price'] ) ) {
        $custom_data  = array() ;
        $custom_data [ 'cfc_pa_color' ]    = isset( $_POST ['cfc_pa_color'] ) ?  sanitize_text_field ( $_POST ['cfc_pa_color'] ) : "" ;

        if(isset( $_POST ['chain_pa_color'] ) && $_POST ['chain_pa_color']!='') {
            $custom_data ['chain_pa_color'] = sanitize_text_field($_POST ['chain_pa_color']);
        }

        if(isset( $_POST ['pick_size'] ) && $_POST ['pick_size']!='') {
            $custom_data ['pick_size'] = sanitize_text_field($_POST ['pick_size']);
        }

        if(isset( $_POST ['cat_size_pa_color'] ) && $_POST ['cat_size_pa_color']!='') {
            $custom_data ['cat_size_pa_color'] = sanitize_text_field($_POST ['cat_size_pa_color']);
        }

        if(isset( $_POST ['cfc_pa_size'] ) && $_POST ['cfc_pa_size']!='') {
            $custom_data ['cfc_pa_size'] = sanitize_text_field($_POST ['cfc_pa_size']);
        }

        if(isset( $_POST ['leather_color'] ) && $_POST ['leather_color']!='') {
            $custom_data ['leather_color'] = sanitize_text_field($_POST ['leather_color']);
        }

        if(isset( $_POST ['rope_color'] ) && $_POST ['rope_color']!='') {
            $custom_data ['rope_color'] = sanitize_text_field($_POST ['rope_color']);
        }

        if(isset( $_POST ['ropebg'] ) && $_POST ['ropebg']!='') {
            $custom_data ['ropebg'] = sanitize_text_field($_POST ['ropebg']);
        }

        if(isset( $_POST ['customer_message'] ) && $_POST ['customer_message']!='') {
            $custom_data ['customer_message'] = sanitize_text_field($_POST ['customer_message']);
        }

        if(isset( $_POST ['client_price'] ) && $_POST ['client_price']!='') {
            $custom_data ['client_price'] = sanitize_text_field($_POST ['client_price']);
        }

        if(isset( $_POST ['client_name'] ) && $_POST ['client_name']!='') {
            $custom_data ['client_name'] = sanitize_text_field($_POST ['client_name']);
        }

        if(isset( $_POST ['client_message'] ) && $_POST ['client_message']!='') {
            $custom_data ['client_message'] = sanitize_text_field($_POST ['client_message']);
        }
        $cart_item_meta ['custom_data']     = $custom_data ;
    }

    return $cart_item_meta;
}
/**
 * Display the custom data on cart and checkout page
 */
add_filter( 'woocommerce_get_item_data', 'get_item_data' , 25, 2 );
function get_item_data ( $other_data, $cart_item ) {
    if ( isset( $cart_item [ 'custom_data' ] ) ) {
        $custom_data  = $cart_item [ 'custom_data' ];

        $other_data[] = array( 'name' => 'Color',
            'display'  => $custom_data['cfc_pa_color'] );

        if($cart_item [ 'custom_data' ]['chain_pa_color']) {
            $other_data[] = array('name' => 'Chain',
                'display' => $custom_data['chain_pa_color']);
        }

        if($cart_item [ 'custom_data' ]['pick_size']) {
            $other_data[] = array('name' => 'Size',
                'display' => $custom_data['pick_size']);
        }

        if($cart_item [ 'custom_data' ]['cat_size_pa_color']) {
            $other_data[] = array('name' => 'Size',
                'display' => $custom_data['cat_size_pa_color']);
        }

        if($cart_item [ 'custom_data' ]['cfc_pa_size']) {
            $other_data[] = array('name' => 'Size',
                'display' => $custom_data['cfc_pa_size']);
        }

        if($cart_item [ 'custom_data' ]['leather_color']) {
            $other_data[] = array('name' => 'Leather',
                'display' => $custom_data['leather_color']);
        }

        if($cart_item [ 'custom_data' ]['rope_color']) {
            $other_data[] = array('name' => 'Rope',
                'display' => '<span class="rope-circle" style="background: '.$custom_data['ropebg'].' "></span>'.$custom_data['rope_color']);
        }

        if($cart_item [ 'custom_data' ]['customer_message']) {
            $other_data[] = array('name' => 'Message',
                'display' => $custom_data['customer_message']);
        }
    }

    return $other_data;
}
/**
 * Add order item meta
 */
add_action( 'woocommerce_add_order_item_meta', 'add_order_item_meta' , 10, 2);
function add_order_item_meta ( $item_id, $values ) {
    if ( isset( $values [ 'custom_data' ] ) ) {
        $custom_data  = $values [ 'custom_data' ];
        wc_add_order_item_meta( $item_id, 'Color', $custom_data['cfc_pa_color'] );


        if($custom_data['chain_pa_color']) {
            wc_add_order_item_meta($item_id, 'Chain', $custom_data['chain_pa_color']);
        }

        if($custom_data['pick_size']) {
            wc_add_order_item_meta($item_id, 'Size', $custom_data['pick_size']);
        }

        if($custom_data['cat_size_pa_color']) {
            wc_add_order_item_meta($item_id, 'Size', $custom_data['cat_size_pa_color']);
        }

        if($custom_data['cfc_pa_size']) {
            wc_add_order_item_meta($item_id, 'Size', $custom_data['cfc_pa_size']);
        }

        if($custom_data['leather_color']) {
            wc_add_order_item_meta($item_id, 'Leather', $custom_data['leather_color']);
        }

        if($custom_data['rope_color']) {
            wc_add_order_item_meta($item_id, 'Rope', $custom_data['rope_color']);
        }

        if($custom_data['client_name']) {
            wc_add_order_item_meta($item_id, 'Client', $custom_data['client_name']);
        }

        if($custom_data['client_price']) {
            wc_add_order_item_meta($item_id, 'Price', $custom_data['client_price']);
        }

        if($custom_data['client_message']) {
            wc_add_order_item_meta($item_id, 'Message', $custom_data['client_message']);
        }
    }
}

add_action( 'woocommerce_before_calculate_totals', 'add_custom_price', 20, 1);
function add_custom_price( $cart ) {
    if(!isset($_SESSION['client_price']) && isset( $_POST ['client_price'] )){
        $_SESSION["client_price"] = $_POST ['client_price'];
    }
    if(isset($_SESSION['client_price'])){

        // This is necessary for WC 3.0+
        if ( is_admin() && ! defined( 'DOING_AJAX' ) )
            return;

        // Avoiding hook repetition (when using price calculations for example)
        if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
            return;
        // Loop through cart items
        foreach ( $cart->get_cart() as $item ) {
            $item['data']->set_price( $_SESSION["client_price"] );
        }
    }
}
?>
