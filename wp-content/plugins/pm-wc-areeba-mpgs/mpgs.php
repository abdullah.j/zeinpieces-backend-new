<?php
/*
Plugin Name: Areeba's MPGS for Woocommerce
Plugin URI: http://pluginsmaker.com/
Description: Areeba's MPGS payment gateway for woocommerce
Version: 1.3
Author: Pluginsmaker.
Author URI: http://pluginsmaker.com
Text Domain: pm-wc-mpgs
Domain Path: /languages/
License: GPL
*/

/**
 * Areeba's MPGS Payment plugin for Woocommerce
 *
 * @package PM/Woocommerce
 * @subpackage Gateways
 */

/**
 * This file is part of Areeba's MPGS For Woocommerce.
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Loads the Payment class for MPGS
 *
 * @since 1.0
 */
function init_pm_wc_mpgs() {

	if ( !class_exists( 'WC_Payment_Gateway' ) ) return;

	load_plugin_textdomain( 'pm-wc-mpgs', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );

	/**
	 * MPGS Payment Gateway
	 *
	 * Provides Hosted Checkout Integration.
	 *
	 * @class 		PM_WC_MPGS
	 * @extends		WC_Payment_Gateway
	 * @version		1.0
	 * @package		PM/Woocommerce/Gateways
	 * @author 		Pluginsmaker
	 */
	class PM_WC_MPGS extends WC_Payment_Gateway {

		/**
		 * Constructor for the gateway.
		 *
		 * @access public
		 * @return void
		 */
		function __construct() {

			// Unique ID for mpgs gateway
			$this->id					= 'mpgs';

			// URL to logo
			$this->icon					= plugins_url( 'images/logo.png', __FILE__ );

			// Direct integration
			$this->has_fields			= false;
			$this->notify_url			= str_replace( 'https:', 'http:', add_query_arg( 'wc-api', 'PM_WC_MPGS', home_url( '/' ) ) );

			// Title of the payment method shown on the admin page
			$this->method_title			= __( 'Areeba\'s mpgs', 'pm-wc-mpgs' );
			$this->method_description	= __( 'Areeba\'s mpgs payment gateway', 'pm-wc-mpgs' );

			// Loads settings fields:
			$this->init_form_fields();
			$this->init_settings();

			// Define user set variables
			$this->title				= $this->get_option( 'title' );
			$this->description			= $this->get_option( 'description' );

			$this->merchant_id			= $this->get_option( 'merchant_id' );
			$this->api_password			= $this->get_option( 'api_password' );
			$this->lightbox				= $this->get_option( 'lightbox' ) == 'yes';

			$this->debug				= $this->get_option( 'debug' ) == 'yes';

			// Logs
			if ( $this->debug ) $this->log = new WC_Logger();

			// Actions
			add_action( 'woocommerce_receipt_' . $this->id							, array( $this, 'receipt_page' ) );
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id	, array( $this, 'process_admin_options' ) );

			// Payment listener/API hook
			add_action( 'woocommerce_api_pm_wc_' . $this->id, array( $this, 'check_notify_response' ) );
		}

		function init_form_fields() {
			$this->form_fields = array(
				'enabled' => array(
					'title'			=> __( 'Enable/Disable', 'woocommerce' ),
					'type'			=> 'checkbox',
					'label'			=> __( 'Enable Areeba\'s mpgs', 'woocommerce' ),
					'default'		=> 'yes'
				),
				'title' => array(
					'title'			=> __( 'Title', 'woocommerce' ),
					'type'			=> 'text',
					'description'	=> __( 'This controls the title which the user sees during checkout.', 'woocommerce' ),
					'default'		=> 'MPGS',
					'desc_tip'		=> true,
				),
				'description' => array(
					'title'			=> __( 'Customer Message', 'woocommerce' ),
					'type'			=> 'textarea',
					'default'		=> 'Pay using Areeba\'s mpgs'
				),

				// MPGS custom fields
				'merchant_id' => array(
					'title'			=> __( 'Merchant ID', 'pm-wc-mpgs' ),
					'type'			=> 'text',
					'default'		=> ''
				),
				'api_password' => array(
					'title'			=> __( 'API Password', 'pm-wc-mpgs' ),
					'type'			=> 'text',
					'default'		=> ''
				),
				'lightbox'		=> array(
					'title'			=> __( 'Lightbox enabled', 'pm-wc-mpgs' ),
					'type'			=> 'checkbox',
					'label'			=> __( 'Enable Lightbox button', 'woocommerce' ),
					'default'		=> 'no',
				),

				'debug' => array(
					'title'			=> __( 'Debug Mode', 'woocommerce' ),
					'type'			=> 'checkbox',
					'label'			=> __( 'Enable test mode and logging', 'woocommerce' ),
					'default'		=> 'no',
					'description'	=> sprintf( __( 'Log MPGS events, such as requests, inside <code>woocommerce/logs/mpgs-%s.txt</code>', 'pm-wc-mpgs' ), sanitize_file_name( wp_hash( 'mpgs' ) ) ),
				)
			);
		}

		/**
		 * Admin Panel Options
		 * - Options for bits like 'title' ...
		 *
		 * @since 1.0
		 */
		public function admin_options() {
?>
			<h3><?php _e( 'MPGS', 'pm-wc-mpgs' ); ?></h3>
			<p><?php _e( 'MPGS works by sending the user to MPGS to enter their payment information.', 'pm-wc-mpgs' ); ?></p>

			<table class="form-table">
			<?php
    			// Generate the HTML For the settings form.
    			$this->generate_settings_html();
			?>
			</table><!--.form-table-->
<?php
		}

		function process_payment( $order_id ) {
			$order		= new WC_Order( $order_id );
			$redirect	= add_query_arg( 'order', $order->id, add_query_arg( 'key', $order->order_key, get_permalink( woocommerce_get_page_id( 'pay' ) ) ) );

			if ( $this->debug ) $this->log->add( 'mpgs', 'process_payment func url=' . $redirect );

			return array(
				'result' 	=> 'success',
				'redirect'	=> $redirect
			);
		}

		function receipt_page( $order_id ) {

			if ( $this->debug ) $this->log->add( 'mpgs', 'receipt_page ' . $order_id );
			if ( $this->debug ) $this->log->add( 'mpgs', print_r( $_REQUEST, true ) );

			$order = new WC_Order( $order_id );

			$currency = get_woocommerce_currency();
			//if ( $currency == 'BCH' ) {
			//	$amount	= number_format( $order->get_total(), 3, '.', '' );
			//} else {
				$amount	= number_format( $order->get_total(), 2, '.', '' );
			//}
			$return_url = $this->notify_url;
			$return_url = add_query_arg( 'order_id', $order_id, $this->notify_url );

			$session_url = "https://ap-gateway.mastercard.com/api/rest/version/48/merchant/{$this->merchant_id}/session";
			$ch	 = curl_init();
			$data = json_encode( array(
				'apiOperation'	=> 'CREATE_CHECKOUT_SESSION',
				'order'			=> array(
					'currency'			=> $currency,
					'id'				=> $order_id,
					'amount'			=> $amount,
					'notificationUrl'	=> $return_url,
				),
				//'merchantId'	=> $this->merchant_id,
			) );

//echo $session_url, '<br>';
//var_dump( $data ); echo '<br><br>';

			curl_setopt( $ch, CURLOPT_URL, $session_url );
			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, $data );
			//curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, false );
			//curl_setopt( $ch, CURLOPT_TIMEOUT, 3000 );
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt( $ch, CURLOPT_HEADER, false );
			curl_setopt( $ch, CURLOPT_USERPWD, 'merchant.' . $this->merchant_id . ':' . $this->api_password );
			//curl_setopt( $ch, CURLOPT_USERPWD, $this->merchant_id . ':' . 'HOWAYTE' );
			//curl_setopt( $ch, CURLOPT_USERPWD, 'HOWAYTE' . ':' . 'HsS%39LeT' );
			//curl_setopt($ch, CURLOPT_SSLVERSION, 3);
			$response = curl_exec( $ch );
			if ( $this->debug ) $this->log->add( 'mpgs', '$response ' . $response );

			$response = json_decode( $response );

			if ( $response->result != 'SUCCESS' || $response->merchant != $this->merchant_id ) {
				if ( $this->debug ) $this->log->add( 'mpgs', 'Cancel:' . $response->result . ' or ' . $response->merchant . '!=' . $this->merchant_id);

				$this->cancel_request( __( 'Error in Create Checkout Session operation', 'pm-wc-mpgs'), $order_id );
			}

//var_dump( $response ); echo '<br><br>';

			add_post_meta( $order_id, '_successIndicator', $response->successIndicator, true );
			$merchant_name = get_bloginfo( 'name' );
			if ( strlen( $merchant_name ) > 40 ) {
				$merchant_name = substr( $merchant_name, 0, 40 );
			}
?>
<script src="https://ap-gateway.mastercard.com/checkout/version/48/checkout.js"
	data-error="errorCallback"
	data-cancel="<?php echo $return_url; ?>"
	data-complete="<?php echo $return_url; ?>"
	data-beforeRedirect="Checkout.saveFormFields"
	data-afterRedirect="Checkout.restoreFormFields">></script>

<p class="waiting_message"><?php _e( 'Waiting...', 'mpgs' ); ?></p>
<?php if ( $this->lightbox ) : ?>
<button onclick="jQuery( '.mpgs-pay-button' ).hide();Checkout.showLightbox();" class="mpgs-pay-button lightbox" style="display:none;"><?php _e( 'Pay via Areeba\' mpgs', 'pm-wc-mpgs' ); ?></button>
<?php else: ?>
<button onclick="jQuery( '.mpgs-pay-button' ).hide();Checkout.showPaymentPage();" class="mpgs-pay-button paymentpage" style="display:none;"><?php _e( 'Pay via Areeba\' mpgs', 'pm-wc-mpgs' ); ?></button>
<?php endif; ?>

<script type="text/javascript">

if ( !window.location.href.endsWith( '#' ) ) {

	function errorCallback( error ) {
		console.log( 'errorCallback' );
		console.log( JSON.stringify( error ) );
	}

	jQuery( function( $ ) {
		Checkout.configure( {
			merchant: '<?php echo $this->merchant_id; ?>',
			session: {
				id: '<?php echo $response->session->id; ?>',
			},
			order: {
				amount: function() {
					// Dynamic calculation of amount
					return <?php echo $amount; ?>;
				},
				currency: '<?php echo $currency; ?>',
				description: '<?php printf( __( 'Purchase from %s (Order No. %s)', 'pm-wc-mpgs' ), get_bloginfo( 'name' ), $order_id ); ?>',
				id: '<?php echo $order_id; ?>',
			},
			billing: {
				address: {
					street: '<?php echo $order->get_billing_address_1(); ?>',
					city: '<?php echo $order->get_billing_city(); ?>',
					postcodeZip: '<?php echo $order->get_billing_postcode(); ?>',
					stateProvince: '<?php echo $order->get_billing_state(); ?>',
					country: '<?php echo $this->convert_country_code( $order->get_billing_country() ); ?>',
				},
			},
			interaction: {
				merchant: {
					name: '<?php echo $merchant_name; ?>',
					/*address: {
						line1: '200 Sample St',
						line2: '1234 Example Town'
					}*/
				},
				locale        : 'en_US',
				theme         : 'default',
				displayControl: {
					billingAddress  : 'OPTIONAL',
					customerEmail   : 'OPTIONAL',
					orderSummary    : 'SHOW_PARTIAL',
					shipping        : 'HIDE',
				}
			},
		} );
		$( '.waiting_message' ).hide();
		$( '.mpgs-pay-button' ).show();
		$( '.mpgs-pay-button' ).click();
		return;
	} );
}
</script>
<?php
		}

		function check_notify_response() {

			if ( $this->debug ) $this->log->add( 'mpgs', 'check_notify_response' );
			if ( $this->debug ) $this->log->add( 'mpgs', print_r( $_GET, true ) );

//echo 'check_notify_response<br>Request:<br>';
//var_dump( $_REQUEST );

			$order_id = isset( $_GET['order_id'] ) ? $_GET['order_id'] : false;
			$resultIndicator = isset( $_GET['resultIndicator'] ) ? $_GET['resultIndicator'] : false;
			$successIndicator = get_post_meta( $order_id, '_successIndicator', true );
			delete_post_meta( $order_id, '_successIndicator' );

			if ( $this->debug ) $this->log->add( 'mpgs', 'order_id=' . $order_id );
			if ( $this->debug ) $this->log->add( 'mpgs', 'resultIndicator=' . $resultIndicator );
			if ( $this->debug ) $this->log->add( 'mpgs', 'successIndicator=' . $successIndicator );
//echo '<br>order_id=', $order_id;
//echo '<br>resultIndicator=', $resultIndicator;
//echo '<br>successIndicator=', $successIndicator;

			if ( $successIndicator == $resultIndicator ) {
				$this->successful_request( $order_id );
			} else {
				$this->cancel_request( 'Result indicator error.', $order_id );
			}
		}

		function cancel_request( $response_txt, $order_id ) {

			if ( $this->debug ) $this->log->add( 'mpgs', 'MPGS: ' . $response_txt . ': ' . $order_id );

			$order = new WC_Order( $order_id );
			$error_message = sprintf( __( 'Payment cancelled: %s', 'pm-wc-mpgs' ), $response_txt );
			$order->cancel_order( $error_message );
			wc_add_notice( $error_message, 'error' );

			$return_url = $order->get_cancel_order_url();
			wp_safe_redirect( $return_url );
			exit();
		}

		function successful_request( $order_id, $response = false ) {

			if ( $this->debug ) $this->log->add( 'mpgs', 'MPGS: payment authorized for Order ID: ' . $order_id );

			$order = new WC_Order( $order_id );
			$order->add_order_note( __( 'Payment Authorized', 'pm-wc-mpgs') );
			$order->payment_complete();
			if ( $response !== false ) {
				wc_add_notice( $response );
			}

			$url = $this->get_return_url( $order );
			if ( $this->debug ) $this->log->add( 'mpgs', 'wp_safe_redirect: ' . $url );
			wp_redirect( $url );
			exit();
		}

		private function convert_country_code( $country ) {
			$countries = array(
			'AF' => 'AFG', //Afghanistan
			'AX' => 'ALA', //&#197;land Islands
			'AL' => 'ALB', //Albania
			'DZ' => 'DZA', //Algeria
			'AS' => 'ASM', //American Samoa
			'AD' => 'AND', //Andorra
			'AO' => 'AGO', //Angola
			'AI' => 'AIA', //Anguilla
			'AQ' => 'ATA', //Antarctica
			'AG' => 'ATG', //Antigua and Barbuda
			'AR' => 'ARG', //Argentina
			'AM' => 'ARM', //Armenia
			'AW' => 'ABW', //Aruba
			'AU' => 'AUS', //Australia
			'AT' => 'AUT', //Austria
			'AZ' => 'AZE', //Azerbaijan
			'BS' => 'BHS', //Bahamas
			'BH' => 'BHR', //Bahrain
			'BD' => 'BGD', //Bangladesh
			'BB' => 'BRB', //Barbados
			'BY' => 'BLR', //Belarus
			'BE' => 'BEL', //Belgium
			'BZ' => 'BLZ', //Belize
			'BJ' => 'BEN', //Benin
			'BM' => 'BMU', //Bermuda
			'BT' => 'BTN', //Bhutan
			'BO' => 'BOL', //Bolivia
			'BQ' => 'BES', //Bonaire, Saint Estatius and Saba
			'BA' => 'BIH', //Bosnia and Herzegovina
			'BW' => 'BWA', //Botswana
			'BV' => 'BVT', //Bouvet Islands
			'BR' => 'BRA', //Brazil
			'IO' => 'IOT', //British Indian Ocean Territory
			'BN' => 'BRN', //Brunei
			'BG' => 'BGR', //Bulgaria
			'BF' => 'BFA', //Burkina Faso
			'BI' => 'BDI', //Burundi
			'KH' => 'KHM', //Cambodia
			'CM' => 'CMR', //Cameroon
			'CA' => 'CAN', //Canada
			'CV' => 'CPV', //Cape Verde
			'KY' => 'CYM', //Cayman Islands
			'CF' => 'CAF', //Central African Republic
			'TD' => 'TCD', //Chad
			'CL' => 'CHL', //Chile
			'CN' => 'CHN', //China
			'CX' => 'CXR', //Christmas Island
			'CC' => 'CCK', //Cocos (Keeling) Islands
			'CO' => 'COL', //Colombia
			'KM' => 'COM', //Comoros
			'CG' => 'COG', //Congo
			'CD' => 'COD', //Congo, Democratic Republic of the
			'CK' => 'COK', //Cook Islands
			'CR' => 'CRI', //Costa Rica
			'CI' => 'CIV', //Côte d\'Ivoire
			'HR' => 'HRV', //Croatia
			'CU' => 'CUB', //Cuba
			'CW' => 'CUW', //Curaçao
			'CY' => 'CYP', //Cyprus
			'CZ' => 'CZE', //Czech Republic
			'DK' => 'DNK', //Denmark
			'DJ' => 'DJI', //Djibouti
			'DM' => 'DMA', //Dominica
			'DO' => 'DOM', //Dominican Republic
			'EC' => 'ECU', //Ecuador
			'EG' => 'EGY', //Egypt
			'SV' => 'SLV', //El Salvador
			'GQ' => 'GNQ', //Equatorial Guinea
			'ER' => 'ERI', //Eritrea
			'EE' => 'EST', //Estonia
			'ET' => 'ETH', //Ethiopia
			'FK' => 'FLK', //Falkland Islands
			'FO' => 'FRO', //Faroe Islands
			'FJ' => 'FIJ', //Fiji
			'FI' => 'FIN', //Finland
			'FR' => 'FRA', //France
			'GF' => 'GUF', //French Guiana
			'PF' => 'PYF', //French Polynesia
			'TF' => 'ATF', //French Southern Territories
			'GA' => 'GAB', //Gabon
			'GM' => 'GMB', //Gambia
			'GE' => 'GEO', //Georgia
			'DE' => 'DEU', //Germany
			'GH' => 'GHA', //Ghana
			'GI' => 'GIB', //Gibraltar
			'GR' => 'GRC', //Greece
			'GL' => 'GRL', //Greenland
			'GD' => 'GRD', //Grenada
			'GP' => 'GLP', //Guadeloupe
			'GU' => 'GUM', //Guam
			'GT' => 'GTM', //Guatemala
			'GG' => 'GGY', //Guernsey
			'GN' => 'GIN', //Guinea
			'GW' => 'GNB', //Guinea-Bissau
			'GY' => 'GUY', //Guyana
			'HT' => 'HTI', //Haiti
			'HM' => 'HMD', //Heard Island and McDonald Islands
			'VA' => 'VAT', //Holy See (Vatican City State)
			'HN' => 'HND', //Honduras
			'HK' => 'HKG', //Hong Kong
			'HU' => 'HUN', //Hungary
			'IS' => 'ISL', //Iceland
			'IN' => 'IND', //India
			'ID' => 'IDN', //Indonesia
			'IR' => 'IRN', //Iran
			'IQ' => 'IRQ', //Iraq
			'IE' => 'IRL', //Republic of Ireland
			'IM' => 'IMN', //Isle of Man
			'IL' => 'ISR', //Israel
			'IT' => 'ITA', //Italy
			'JM' => 'JAM', //Jamaica
			'JP' => 'JPN', //Japan
			'JE' => 'JEY', //Jersey
			'JO' => 'JOR', //Jordan
			'KZ' => 'KAZ', //Kazakhstan
			'KE' => 'KEN', //Kenya
			'KI' => 'KIR', //Kiribati
			'KP' => 'PRK', //Korea, Democratic People\'s Republic of
			'KR' => 'KOR', //Korea, Republic of (South)
			'KW' => 'KWT', //Kuwait
			'KG' => 'KGZ', //Kyrgyzstan
			'LA' => 'LAO', //Laos
			'LV' => 'LVA', //Latvia
			'LB' => 'LBN', //Lebanon
			'LS' => 'LSO', //Lesotho
			'LR' => 'LBR', //Liberia
			'LY' => 'LBY', //Libya
			'LI' => 'LIE', //Liechtenstein
			'LT' => 'LTU', //Lithuania
			'LU' => 'LUX', //Luxembourg
			'MO' => 'MAC', //Macao S.A.R., China
			'MK' => 'MKD', //Macedonia
			'MG' => 'MDG', //Madagascar
			'MW' => 'MWI', //Malawi
			'MY' => 'MYS', //Malaysia
			'MV' => 'MDV', //Maldives
			'ML' => 'MLI', //Mali
			'MT' => 'MLT', //Malta
			'MH' => 'MHL', //Marshall Islands
			'MQ' => 'MTQ', //Martinique
			'MR' => 'MRT', //Mauritania
			'MU' => 'MUS', //Mauritius
			'YT' => 'MYT', //Mayotte
			'MX' => 'MEX', //Mexico
			'FM' => 'FSM', //Micronesia
			'MD' => 'MDA', //Moldova
			'MC' => 'MCO', //Monaco
			'MN' => 'MNG', //Mongolia
			'ME' => 'MNE', //Montenegro
			'MS' => 'MSR', //Montserrat
			'MA' => 'MAR', //Morocco
			'MZ' => 'MOZ', //Mozambique
			'MM' => 'MMR', //Myanmar
			'NA' => 'NAM', //Namibia
			'NR' => 'NRU', //Nauru
			'NP' => 'NPL', //Nepal
			'NL' => 'NLD', //Netherlands
			'AN' => 'ANT', //Netherlands Antilles
			'NC' => 'NCL', //New Caledonia
			'NZ' => 'NZL', //New Zealand
			'NI' => 'NIC', //Nicaragua
			'NE' => 'NER', //Niger
			'NG' => 'NGA', //Nigeria
			'NU' => 'NIU', //Niue
			'NF' => 'NFK', //Norfolk Island
			'MP' => 'MNP', //Northern Mariana Islands
			'NO' => 'NOR', //Norway
			'OM' => 'OMN', //Oman
			'PK' => 'PAK', //Pakistan
			'PW' => 'PLW', //Palau
			'PS' => 'PSE', //Palestinian Territory
			'PA' => 'PAN', //Panama
			'PG' => 'PNG', //Papua New Guinea
			'PY' => 'PRY', //Paraguay
			'PE' => 'PER', //Peru
			'PH' => 'PHL', //Philippines
			'PN' => 'PCN', //Pitcairn
			'PL' => 'POL', //Poland
			'PT' => 'PRT', //Portugal
			'PR' => 'PRI', //Puerto Rico
			'QA' => 'QAT', //Qatar
			'RE' => 'REU', //Reunion
			'RO' => 'ROU', //Romania
			'RU' => 'RUS', //Russia
			'RW' => 'RWA', //Rwanda
			'BL' => 'BLM', //Saint Barth&eacute;lemy
			'SH' => 'SHN', //Saint Helena
			'KN' => 'KNA', //Saint Kitts and Nevis
			'LC' => 'LCA', //Saint Lucia
			'MF' => 'MAF', //Saint Martin (French part)
			'SX' => 'SXM', //Sint Maarten / Saint Matin (Dutch part)
			'PM' => 'SPM', //Saint Pierre and Miquelon
			'VC' => 'VCT', //Saint Vincent and the Grenadines
			'WS' => 'WSM', //Samoa
			'SM' => 'SMR', //San Marino
			'ST' => 'STP', //S&atilde;o Tom&eacute; and Pr&iacute;ncipe
			'SA' => 'SAU', //Saudi Arabia
			'SN' => 'SEN', //Senegal
			'RS' => 'SRB', //Serbia
			'SC' => 'SYC', //Seychelles
			'SL' => 'SLE', //Sierra Leone
			'SG' => 'SGP', //Singapore
			'SK' => 'SVK', //Slovakia
			'SI' => 'SVN', //Slovenia
			'SB' => 'SLB', //Solomon Islands
			'SO' => 'SOM', //Somalia
			'ZA' => 'ZAF', //South Africa
			'GS' => 'SGS', //South Georgia/Sandwich Islands
			'SS' => 'SSD', //South Sudan
			'ES' => 'ESP', //Spain
			'LK' => 'LKA', //Sri Lanka
			'SD' => 'SDN', //Sudan
			'SR' => 'SUR', //Suriname
			'SJ' => 'SJM', //Svalbard and Jan Mayen
			'SZ' => 'SWZ', //Swaziland
			'SE' => 'SWE', //Sweden
			'CH' => 'CHE', //Switzerland
			'SY' => 'SYR', //Syria
			'TW' => 'TWN', //Taiwan
			'TJ' => 'TJK', //Tajikistan
			'TZ' => 'TZA', //Tanzania
			'TH' => 'THA', //Thailand    
			'TL' => 'TLS', //Timor-Leste
			'TG' => 'TGO', //Togo
			'TK' => 'TKL', //Tokelau
			'TO' => 'TON', //Tonga
			'TT' => 'TTO', //Trinidad and Tobago
			'TN' => 'TUN', //Tunisia
			'TR' => 'TUR', //Turkey
			'TM' => 'TKM', //Turkmenistan
			'TC' => 'TCA', //Turks and Caicos Islands
			'TV' => 'TUV', //Tuvalu     
			'UG' => 'UGA', //Uganda
			'UA' => 'UKR', //Ukraine
			'AE' => 'ARE', //United Arab Emirates
			'GB' => 'GBR', //United Kingdom
			'US' => 'USA', //United States
			'UM' => 'UMI', //United States Minor Outlying Islands
			'UY' => 'URY', //Uruguay
			'UZ' => 'UZB', //Uzbekistan
			'VU' => 'VUT', //Vanuatu
			'VE' => 'VEN', //Venezuela
			'VN' => 'VNM', //Vietnam
			'VG' => 'VGB', //Virgin Islands, British
			'VI' => 'VIR', //Virgin Island, U.S.
			'WF' => 'WLF', //Wallis and Futuna
			'EH' => 'ESH', //Western Sahara
			'YE' => 'YEM', //Yemen
			'ZM' => 'ZMB', //Zambia
			'ZW' => 'ZWE', //Zimbabwe
			);
			$iso_code = isset( $countries[$country] ) ? $countries[$country] : $country;
			return $iso_code;
		}
	}
}

add_action( 'plugins_loaded', 'init_pm_wc_mpgs' );

/**
 * Adds class 'PM_WC_MPGS' to the mathods list.
 *
 * As well as defining your class, you need to also tell WC that it exists
 *
 * @since 1.0
 * @param array $methods, list of classes
 */
function pm_add_wc_mpgs_class( $methods ) {
	if ( class_exists( 'WC_Payment_Gateway' ) ) {
		$methods[] = 'PM_WC_MPGS';
	}
	return $methods;
}

add_filter( 'woocommerce_payment_gateways', 'pm_add_wc_mpgs_class' );
