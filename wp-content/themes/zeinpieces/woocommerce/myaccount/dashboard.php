<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

$allowed_html = array(
    'a' => array(
        'href' => array(),
    ),
);

?>
    <div class="hello-text">
        <div class="container">
            <p class="wel-text">Hello <b><?php echo $current_user->display_name ?></b>, welcome To Zein Pieces</p>
            <p class="log-text"><i>Not <b><?php echo $current_user->display_name ?></b>?</i>
                <a href="<?php echo wp_logout_url( get_permalink( wc_get_page_id( 'myaccount' ) ) ) ?>" class="login-t"><i>Log out</i></a>
            </p>

            <p class="acc-dashbord">Your account dashboard allows you to view your recent orders, manage your shipping and billing addresses, and edit your password and account details.</p>
        </div>
    </div>
    <div class="faq-sec">
        <div class="container">
            <div class="faq-drawer">
                <input class="faq-drawer__trigger" id="faq-drawer" type="checkbox" /><label class="faq-drawer__title" for="faq-drawer">PERSONAL INFORMATION </label>
                <div class="faq-drawer__content-wrapper">
                    <div class="faq-drawer__content">
                        <div class="container">
                            <div class="form3">
                                <?php echo do_shortcode('[woocommerce_edit_account]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="faq-drawer">
                <input class="faq-drawer__trigger" id="faq-drawer-2" type="checkbox" /><label class="faq-drawer__title" for="faq-drawer-2">MY ADDRESSES</label>
                <div class="faq-drawer__content-wrapper">
                    <div class="faq-drawer__content">
                        <div class="container">
                            <div class="form3">
                                <?php echo do_shortcode('[woocommerce_address]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="faq-drawer">
                <input class="faq-drawer__trigger" id="faq-drawer-3" type="checkbox" /><label class="faq-drawer__title" for="faq-drawer-3">MY ORDER</label>
                <div class="faq-drawer__content-wrapper">
                    <div class="faq-drawer__content">
                        <div class="container">
                            <div class="form3">
                                <?php echo do_shortcode('[woocommerce_orders]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="faq-drawer">
                <input class="faq-drawer__trigger" id="faq-drawer-4" type="checkbox" /><label class="faq-drawer__title" for="faq-drawer-4">MY GIFT CARDS</label>
                <div class="faq-drawer__content-wrapper">
                    <div class="faq-drawer__content">
                        <div class="container">
                            <div class="form3">
                                <?php echo get_template_part('/yith-woocommerce-gift-cards-premium/templates/myaccount/my-giftcards'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
/**
 * My Account dashboard.
 *
 * @since 2.6.0
 */
do_action( 'woocommerce_account_dashboard' );

/**
 * Deprecated woocommerce_before_my_account action.
 *
 * @deprecated 2.6.0
 */
do_action( 'woocommerce_before_my_account' );

/**
 * Deprecated woocommerce_after_my_account action.
 *
 * @deprecated 2.6.0
 */
do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
