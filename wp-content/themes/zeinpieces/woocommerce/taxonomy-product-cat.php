<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/taxonomy-product-cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     4.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$category_slug = get_queried_object()->slug;
?>

    <div class="category-menu">
        <div class="container">
            <?php show_the_breadcrumbs(); ?>
        </div>
    </div>

    <div class="main">
        <div class="container" style="margin-bottom: 30px;">
            <h2 class="kalimat-text"><?php echo $collection->name; ?></h2>
            <?php echo term_description( $collection->term_id, 'category' ); ?>
        </div>
        <div class="container">
            <div class="category-sec-main">
                <div class="select-category">
                    <div class="check-box-select">
                        <div class="filter-head">
                            <h4>Filter by</h4>
                        </div>
                        <?php echo do_shortcode("[wcpf_filters id='4550']") ?>
                    </div>
                    <div class="category-select">
                        <?php echo do_shortcode("[products category='$category_slug' columns='3' paginate='true' filter-id='4550']") ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();