<?php
/**
 * Shop breadcrumb
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/breadcrumb.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 * @see         woocommerce_breadcrumb()
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$product_id = get_the_ID();
$product = get_post( $product_id );
$slug = $product->post_name;
$title = $product->post_title;
?>
<?php
if($slug == 'digital-gift-voucher' || $slug == 'physical-gift-voucher'){ ?>
    <div class="category-menu">
        <div class="container">
            <ul class="breadcrumbs">
                <li>
                    <a href="<?php echo get_home_url(); ?>">Home</a>
                </li>
                <li>
                    <a href="<?php echo get_permalink( get_page_by_path( 'gift-card' ) ) ?>">Gift Card</a>
                </li>
                <li><?php echo $title; ?></li>
            </ul>
        </div>
    </div>
<?php }else{ ?>
    <div class="category-menu">
        <div class="container">
            <ul class="breadcrumbs">
                <?php
                foreach ( $breadcrumb as $key => $crumb ) {

                    echo $before;

                    if ( ! empty( $crumb[1] ) && sizeof( $breadcrumb ) !== $key + 1 ) {
                        echo '<li><a href="' . esc_url( $crumb[1] ) . '" class="hover-secondary">' . esc_html( $crumb[0] ) . '</a></li>';
                    } else {
                        echo '<li><span>'.esc_html( $crumb[0] ).'</span></li>';
                    }

                    echo $after;

                    if ( sizeof( $breadcrumb ) !== $key + 1 ) {
//							echo $delimiter;
                    }
                }
                ?>
            </ul>
        </div>
    </div>
<?php } ?>