<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.

	return;
}
?>
<?php $price = get_post_meta( get_the_ID(), '_price', true ); ?>
            
<style>
    .stock.in-stock, .qty, .quantity, .product-info .product-price{
        display:none!important;
    }
    
    table{
        width:100%;
        text-align:left;
    }
    
    table label{
        text-align:left;
    }
    
    #client_name, #client_price {
        width: 170px;
        background-color: #f6f5f5;
        padding: 14px 15px;
        font-family: "FuturaStd",sans-serif;
        font-size: 10px;
        line-height: 18px;
        font-size: .71428571rem;
        font-weight: 500;
        letter-spacing: 1px;
        line-height: 11px;
        text-transform: uppercase;
        border: 0;
        border-radius: 0 !important;
    }
</style>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>
    <div class="container">
        <div class="row">

			<div class="col-xl-3"></div>

            <div class="col-xl-6 col-lg-12" style="text-align:center;">
                <div class="product-info">
					<?php
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_meta - 40
					 */
					do_action( 'woocommerce_single_product_summary' );
					?>
                </div>
            </div>
            <div class="col-xl-3"></div>
        </div>
    </div>

	<?php do_action( 'woocommerce_after_single_product' ); ?>
</div>
