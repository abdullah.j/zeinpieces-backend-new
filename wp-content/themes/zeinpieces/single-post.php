<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package zeinpieces
 */

$post_id   = get_the_ID();
setup_postdata( $post_id );
$category = wp_get_post_categories($post_id);

$related_posts_args = array(
    'post_type'      => 'post',
    'posts_per_page' => 3,
    'fields'         => 'ids',
    'category__in'   => $category,
    'post__not_in'   => array( $post_id )
);
$related_posts = get_posts( $related_posts_args );
$link = get_permalink( $post_id );

get_header();
?>

    <div class="category-menu">
        <div class="container">
            <?php show_the_breadcrumbs();?>
        </div>
    </div>
    <div class="main">
        <div class="imp-bog-heading">
            <div class="container">
                <div class="importance-section">
                    <div class="importance-icon"></div>
                    <div class="importance-img">
                        <p class="date-text"><?php echo get_the_date('M d.Y', $post_id); ?></p>
                        <h2 class="heading-importance"><?php the_title(); ?></h2>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row-imp">
                    <div class="left-imp-blog-section">
                        <div class="col-icon-blog">
                            <div class="social-blog-icon">
                                <div class="facebook-icon">
                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>"><img src="<?php echo THEME_URL; ?>/assets/images/i-6.png"></a>
                                </div>
                                <div class="twitter-icon">
                                    <a target="_blank" href="https://twitter.com/home?status=Checkout the '<?php echo get_the_title($post_id); ?>"><img src="<?php echo THEME_URL; ?>/assets/images/twiter.png"></a>
                                </div>
                                <div class="google-plus-icon">
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo $link; ?>"><img src="<?php echo THEME_URL; ?>/assets/images/google-plus.png"></a>
                                </div>
                                <div class="pintrest-icon">
                                    <a target="_blank" href="https://pinterest.com/pin/create/button/?url=ZeinPieces&media=<?php echo get_the_post_thumbnail_url($post_id, 'thumbnail-670-420'); ?>"><img src="<?php echo THEME_URL; ?>/assets/images/i-8.png"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-img-blog">
                            <?php echo get_the_post_thumbnail( $post_id, 'thumbnail-700-500' );?>
                        </div>
                    </div>
                    <div class="right-blank-sec"></div>
                </div>
            </div>
        </div>
        <div class="imp-blog-text">
            <div class="container">
                <div class="importance-section">
                    <div class="importance-icon"></div>
                    <div class="importance-img">
                        <div class="imp-p-text">
                            <?php the_content(); ?>
                            <div class="atricle-share">
                                <div class="share-heading">
                                    <p>Share this article </p>
                                </div>
                                <div class="article-icon">
                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $link; ?>"><img src="<?php echo THEME_URL; ?>/assets/images/i-6.png"></a>
                                    <a target="_blank" href="https://twitter.com/home?status=Checkout the '<?php echo get_the_title($post_id); ?>"><img src="<?php echo THEME_URL; ?>/assets/images/twiter.png"></a>
                                    <a target="_blank" href="https://plus.google.com/share?url=<?php echo $link; ?>"><img src="<?php echo THEME_URL; ?>/assets/images/google-plus.png"></a>
                                    <a target="_blank" href="https://pinterest.com/pin/create/button/?url=ZeinPieces&media=<?php echo get_the_post_thumbnail_url($post_id, 'thumbnail-670-420'); ?>"><img src="<?php echo THEME_URL; ?>/assets/images/i-8.png"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="save-blog-sec">
            <div class="container">
                <div class="phy-gift-heading">
                    <h1 class="gift-recomended-sec">more to read</h1>
                </div>
            </div>
            <?php if($category): ?>
                <div class="main-must-gallery">
                    <div class="container">
                        <div class="must-gallery-sec">
                            <?php foreach ( $related_posts as $post_id ) { ?>
                                <?php the_post_block_two( $post_id ) ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <?php echo get_template_part('partials/subscribe') ?>
    </div>
<?php
get_footer();