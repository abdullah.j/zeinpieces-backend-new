<?php
/**
 * zeinpieces functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package zeinpieces
 */

if ( !defined('THEME_URL') ) {
    define ( 'THEME_URL', get_template_directory_uri() );
}

include_once 'lib/functions-breadcrumb.php';
include_once 'lib/functions-enqueue.php';
include_once 'lib/functions-helpers.php';
include_once 'lib/functions-menu.php';
include_once 'lib/functions-post-types.php';
include_once 'lib/functions-settings.php';
include_once 'lib/functions-shortcodes.php';
include_once 'lib/functions-partials.php';
include_once 'lib/functions-actions.php';
include_once 'lib/functions-register-fields.php';
include_once 'lib/functions-ajax-login-init.php';
include_once 'lib/functions-customized-pieces.php';

/**************** Remove removes extra scripts ******************/
add_action('after_setup_theme', 'zeinpieces_setup');
function zeinpieces_setup () {
    remove_action('admin_print_styles', 'print_emoji_styles');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_filter('wp_mail', 'wp_staticize_emoji_for_email');
    remove_filter('the_content_feed', 'wp_staticize_emoji');
    remove_filter('comment_text_rss', 'wp_staticize_emoji');
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10);

    remove_filter( 'pre_term_description', 'wp_filter_kses' );
    remove_filter( 'term_description', 'wp_kses_data' );
}
/*****************************************************************/


/*************************** Images ******************************/
add_action( 'after_setup_theme', 'zeinpieces_after_setup' );
function zeinpieces_after_setup() {
    add_theme_support('post-thumbnails');
    add_theme_support('post-formats', array( 'gallery', 'video' ));
    add_image_size('medium', 350, 350, true);
    add_image_size('thumbnail-670-420', 670, 420, true);
    add_image_size('thumbnail-255-260', 255, 260, true);
    add_image_size('thumbnail-349-450', 349, 450, true);
    add_image_size('thumbnail-369-369', 369, 369, true);
    add_image_size('thumbnail-1108-601', 1108, 601, true);
    add_image_size('thumbnail-347-230', 347, 230, true);
    add_image_size('thumbnail-487-680', 487, 680, true);
    add_image_size('thumbnail-361-272', 361, 272, true);
    add_image_size('thumbnail-551-415', 551, 415, true);
    add_image_size('thumbnail-700-500', 700, 500, true);
    add_image_size('thumbnail-538-300', 538, 300, true);
    add_image_size('thumbnail-202-260', 202, 260, true);
    add_image_size('thumbnail-552-500', 552, 500, true);
    add_image_size('thumbnail-155-260', 155, 260, true);
    add_image_size('thumbnail-480-364', 480, 364, true);
    add_image_size('large', 1110, 1110, true);
}
/*****************************************************************/

/*************************** WooCommerce *************************/
add_action( 'after_setup_theme', 'zeinpieces_woocommerce_support' );
function zeinpieces_woocommerce_support() {
    add_theme_support( 'woocommerce' );

    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating');
    remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt');
}
/*****************************************************************/

// todo: change in CSS: .page:not(.no-top-padding) to body .page:not(.no-top-padding)
// todo: and remove this filter
add_filter('body_class','zp_body_class');
function zp_body_class($classes) {
    if (($key = array_search('page', $classes)) !== false) {
        unset($classes[$key]);
    }
    return $classes;
}

add_filter( 'woocommerce_currency_symbol', 'change_currency_symbol', 10, 2 );
function change_currency_symbol( $symbols, $currency ) {
    // change the currency symbol for US Dollar from $ to USD
    if ( 'USD' === $currency ) {
        return 'USD';
    }

    return $symbols;
}

function custom_excerpt_length() {
    return 5;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function wpdocs_excerpt_more() {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

function add_areeba_logo() {
    echo '<img src='. THEME_URL.'/assets/img/powered-by-2.png" class="areeba-logo" alt="Power by Areeba" />';
}

add_action( 'woocommerce_after_checkout_form', 'add_areeba_logo');


// Function to display some text at the end of a post

function amount_text() {
    return 'Amount (USD)';
}

add_filter( 'yith_gift_cards_template_amount_title', 'amount_text');


function quantity_wp_head() {

    if ( is_product() && (has_term( 'Personalized pieces', 'collection'))) {
        ?>
        <style type="text/css">.woocommerce div.product form.cart { width:0; height:0; display: none; visibility: hidden; }</style>
        <?php

        add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_single_add_to_cart_text' );  // 2.1 +

        function woo_custom_single_add_to_cart_text() {

            return __( 'ADD TO CART', 'woocommerce' );

        }



    }


}
add_action( 'wp_head', 'quantity_wp_head' );

function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

add_filter( 'template_include', 'custom_single_product_template_include', 50, 1 );
function custom_single_product_template_include( $template ) {
    if ( is_singular('product') && (has_term( 'custom', 'product_cat')) ) {
        $template = get_stylesheet_directory() . '/woocommerce/single-product-custom.php';
    }
    return $template;
}

add_action( 'woocommerce_thankyou', 'your_func', 10, 1 );

function your_func($order_id) {

    $order = new WC_Order( $order_id );
    /* Do Something with order ID */
    unset($_SESSION["client_price"]);
}

add_action( 'woocommerce_order_status_cancelled', 'change_status_to_refund', 10, 1 );

function change_status_to_refund( $order_id ){
    //Do Something here
    unset($_SESSION["client_price"]);
}


add_filter ('add_to_cart_redirect', 'redirect_to_checkout');

function redirect_to_checkout() {
    if(isset($_SESSION['client_price'])){
        return WC()->cart->get_checkout_url();
    }
}




// Part 1
// Add the message notification and place it over the billing section
// The "display:none" hides it by default

add_action( 'woocommerce_before_checkout_billing_form', 'bbloomer_echo_notice_shipping' );

function bbloomer_echo_notice_shipping() {
    echo '<div class="shipping-notice woocommerce-info" style="display:none">Please allow 5-10 business days for delivery after order processing.</div>';
}

// Part 2
// Show or hide message based on billing country
// The "display:none" hides it by default



add_action( 'woocommerce_review_order_after_order_total', 'review_order_after_order_total_callback' );
function review_order_after_order_total_callback(){
    $text = __('Learn more about our shipping rates.');

    ?><tr class="order-total"><th colspan="2"><a class="cservice" href="/customer-service/"><?php echo $text; ?></a></th></tr><?php
}

/*******************ADDED BY CARLA**************************/
// Code to clear default shipping option.
add_filter( 'woocommerce_shipping_chosen_method', '__return_false', 99);
// Validate shipping method fields and returning an error if none is choosed


add_action('wp_footer', 'billing_country_update_checkout', 50);
function billing_country_update_checkout() {
    if ( ! is_checkout() ) return;
    ?>
    <script type="text/javascript">
        jQuery( document.body ).on( 'updated_checkout', function(){
            console.log('on updated_checkout: function fired');
            var amount =   jQuery('.woocommerce-shipping-totals span.woocommerce-Price-amount.amount').text();
            var country = jQuery( "#billing_country option:selected" ).text();
            jQuery('.woocommerce ul#shipping_method li:nth-child(2) label').css("display","none");

            jQuery('.woocommerce ul#shipping_method li:nth-child(2)').append('<label class="onload-css">Delivery <span class="tintext">(Delivery charge to ' + country + ' is  ' +amount+ ')</span></label>');
        });
        jQuery(function($){
            $('select#billing_country, select#shipping_country').on( 'change', function (){
                var amount =   jQuery('.woocommerce-shipping-totals span.woocommerce-Price-amount.amount').text();
                var country = jQuery( "#billing_country option:selected" ).text();
                var t = { updateTimer: !1,  dirtyInput: !1,
                    reset_update_checkout_timer: function() {
                        clearTimeout(t.updateTimer)
                    },
                    trigger_update_checkout: function() {
                        t.reset_update_checkout_timer(), t.dirtyInput = !1,
                            $(document.body).trigger("update_checkout")
                    }
                };
                $(document.body).trigger('update_checkout');
                console.log('Event: update_checkout');
                $('tr.woocommerce-shipping-totals.shipping li:nth-child(2) label').hide();

                $('tr.woocommerce-shipping-totals.shipping li:nth-child(2)').append('<label class="onload-css">Delivery <span class="tintext">(Delivery charge to ' + country + ' is  ' +amount+ ')</span></label>');
            });
        });
    </script>
    <?php
}


add_action('wp_footer', 'cart_update_country', 60);
function cart_update_country() {
    if ( ! is_cart() ) return;
    ?>
    <script type="text/javascript">

        jQuery( document ).ready(function() {
            var amount =   jQuery('.woocommerce-shipping-totals span.woocommerce-Price-amount.amount').text();
            var country = '<?php echo WC()->customer->get_shipping_country() ?>' ;
            if(country=='LB') country = 'Lebanon';
            jQuery('.woocommerce ul#shipping_method li:nth-child(2) label').css("display","none");

            jQuery('.woocommerce ul#shipping_method li:nth-child(2)').append('<label class="onload-css">Delivery <span class="tintext">(Delivery charge to ' + country + ' is  ' +amount+ ')</span></label>');
        });

        jQuery( document.body ).on( 'updated_cart_totals', function(){
           // console.log('on updated_checkout: function fired');
            var amount =   jQuery('.woocommerce-shipping-totals span.woocommerce-Price-amount.amount').text();
            var country = '<?php echo WC()->customer->get_shipping_country() ?>' ;
            if(country=='LB') country = 'Lebanon';
            jQuery('.woocommerce ul#shipping_method li:nth-child(2) label').css("display","none");

            jQuery('.woocommerce ul#shipping_method li:nth-child(2)').append('<label class="onload-css">Delivery <span class="tintext">(Delivery charge to ' + country + ' is  ' +amount+ ')</span></label>');
        });

    </script>
    <?php
}
/*******************ADDED BY CARLA**************************/


// Adding a custom checkout date field

add_filter( 'woocommerce_billing_fields', 'add_birth_date_billing_field', 20, 1 );
function add_birth_date_billing_field($billing_fields) {

    $billing_fields['billing_birth_date'] = array(
        'type'        => 'date',
        'label'       => __('Date of Birth'),
        'class'       => array('form-row-wide'),
        'priority'    => 55,
        'required'    => true,
        'clear'       => true,
    );
    return $billing_fields;
}
// Add Variation Gender to Menu
add_filter('woocommerce_attribute_show_in_nav_menus', 'wc_reg_for_menus', 1, 2);

function wc_reg_for_menus( $register, $name = '' ) {
    if ( $name == 'pa_gender' ) $register = true;
    return $register;
}

add_filter( 'woocommerce_shortcode_products_query', 'extend_products_shortcode_to_collection', 10, 3 );
function extend_products_shortcode_to_collection( $query_args, $atts, $loop_name ){
    if ( ! empty($atts['class']) && strpos($atts['class'], 'collection') !== false ) {
        global $wpdb;

        $terms = array_map( 'sanitize_title', explode( ',', $atts['class'] ) );
        array_shift( $terms );
        $terms = implode(',', $terms);
        $terms = str_replace(",", "','", $terms);

        $ids = $wpdb->get_col( "
            SELECT DISTINCT tr.object_id
            FROM {$wpdb->prefix}term_relationships as tr
            INNER JOIN {$wpdb->prefix}term_taxonomy as tt ON tr.term_taxonomy_id = tt.term_taxonomy_id
            INNER JOIN {$wpdb->prefix}terms as t ON tt.term_id = t.term_id
            WHERE tt.taxonomy LIKE 'collection' AND t.slug IN ('$terms')
        " );

        if ( ! empty( $ids ) ) {
            if ( 1 === count( $ids ) ) {
                $query_args['p'] = $ids[0];
            } else {
                $query_args['post__in'] = $ids;
            }
        }
    }
    return $query_args;
}

//Wish list button
add_action( 'woocommerce_after_add_to_cart_button', 'add_custom_button', 10, 0 );
function add_custom_button() {
    $my_custom_link = home_url();
    echo '<div class="wishlist">'.do_shortcode('[ti_wishlists_addtowishlist]').'</div>';
};
//add remove product in checkout page
/**
 * Allows to remove products in checkout page.
 *
 * @param string $product_name
 * @param array $cart_item
 * @param string $cart_item_key
 * @return string
 */
function lionplugins_woocommerce_checkout_remove_item( $product_name, $cart_item, $cart_item_key ) {
    if ( is_checkout() ) {
        $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

        $remove_link = apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
            '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s">×</a>',
            esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
            __( 'Remove this item', 'woocommerce' ),
            esc_attr( $product_id ),
            esc_attr( $_product->get_sku() )
        ), $cart_item_key );

        return '<span>' . $remove_link . '</span> <span>' . $product_name . '</span>';
    }

    return $product_name;
}
add_filter( 'woocommerce_cart_item_name', 'lionplugins_woocommerce_checkout_remove_item', 10, 3 );

//change no products found message
add_action( 'woocommerce_no_products_found', function(){
    remove_action( 'woocommerce_no_products_found', 'wc_no_products_found', 10 );

    // HERE change your message below
    $message = __( 'No products were found matching your selection.', 'woocommerce' );

    echo '<div class="container" style="margin-top: 10px;"><p class="woocommerce-info">' . $message .'</p></div>';
}, 9 );

add_filter('woocommerce_checkout_fields', 'custom_override_checkout_fields');
function custom_override_checkout_fields($fields)
{
    $fields['billing']['billing_wooccm14']['placeholder'] = 'Business Name';
    return $fields;
}