<?php
/**
 * The template for displaying Blog posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zeinpieces
 */
$must_read_posts_args = array(
    'post_type'         => 'post',
    'posts_per_page'    => 2,
    'category_name'     => 'must-read',
    'fields'            => 'ids'
);
$latest_must_read_posts = get_posts($must_read_posts_args);

$jewelry_posts_args = array(
    'post_type'         => 'post',
    'posts_per_page'    => 3,
    'category_name'     => 'jewelry',
    'fields'            => 'ids'
);
$latest_jewelry_posts = get_posts($jewelry_posts_args);

$inspiration_posts_args = array(
    'post_type'         => 'post',
    'posts_per_page'    => 3,
    'category_name'     => 'inspiration',
    'fields'            => 'ids'
);
$latest_inspiration_posts = get_posts($inspiration_posts_args);

$latest_press_args = array(
    'post_type'         => 'post',
    'posts_per_page'    => 3,
    'category_name'     => 'press',
    'fields'            => 'ids'
);
$latest_press_posts = get_posts($latest_press_args);

$count_posts = wp_count_posts();

get_header();
?>

<div class="category-menu">
    <div class="container">
        <?php show_the_breadcrumbs();?>
    </div>
</div>

<div class="'main">
    <div class="bog-heading">
        <div class="container">
            <h2 class="kalimat-text">OUR BLOG</h2>
        </div>
    </div>
    <?php if($latest_must_read_posts): ?>
        <div class="must-reads-sec">
        <div class="container">
            <h3 class="must-heading">must reads</h3>
        </div>
        <div class="main-must-gallery">
            <div class="container">
                <div class="must-gallery-sec">
                    <?php
                    foreach ( $latest_must_read_posts as $post_id ) {
                        the_post_block_one( $post_id );
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if($latest_jewelry_posts): ?>
        <div class="jewelry-section">
            <div class="container">
                <h3 class="must-heading">jewelry BLOG</h3>
            </div>
            <div class="main-must-gallery">
                <div class="container">
                    <div class="must-gallery-sec">
                        <?php
                        foreach ( $latest_jewelry_posts as $post_id ) {
                            the_post_block_two( $post_id );
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="slide-btn2">
                <a href="<?php echo get_category_link( get_cat_ID( 'jewelry' ) ) ?>" class="btn-load-more">View more</a>
            </div>
        </div>
    <?php endif; ?>
    <?php if($latest_inspiration_posts): ?>
        <div class="inspirational-section">
        <div class="container">
            <h3 class="must-heading">inspirational BLOG</h3>
        </div>
        <div class="main-must-gallery">
            <div class="container">
                <div class="must-gallery-sec">
                    <?php
                    foreach ( $latest_inspiration_posts as $post_id ) {
                        the_post_block_two( $post_id );
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="slide-btn2">
            <a href="<?php echo get_category_link( get_cat_ID( 'inspiration' ) ) ?>" class="btn-load-more">view more</a>
        </div>
    </div>
    <?php endif; ?>
    <?php if($latest_press_posts): ?>
        <div class="inspirational-section">
        <div class="container">
            <h3 class="must-heading">the press</h3>
        </div>
        <div class="main-must-gallery">
            <div class="container">
                <div class="must-gallery-sec">
                    <?php
                    foreach ( $latest_press_posts as $post_id ) {
                        the_post_block_two( $post_id );
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="slide-btn2">
            <a href="<?php echo get_category_link( get_cat_ID( 'press' ) ) ?>" class="btn-load-more">view more</a>
        </div>
    </div>
    <?php endif; ?>
    <?php echo get_template_part('partials/subscribe') ?>
</div>

<?php
get_footer();
