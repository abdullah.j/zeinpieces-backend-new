<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package zeinpieces
 */

get_header();
?>

<div class="page-404">
    <div class="category-menu">
        <div class="container">
            <?php show_the_breadcrumbs();?>
        </div>
    </div>

    <div class="container">
        <h1 class="inner-page-title large-title">404</h1>
        <div class="text-center">
            <a href="<?php echo get_home_url(); ?>" class="inner-page-link text-uppercase hover-secondary">return to homepage</a>
        </div>

        <h2>The requested resource does not exist.</h2>

        <p>The requested resource does not exist. The link you followed is either outdated, inaccurate, or the server has been instructed not to let you have it. If a problem occurs, do not hesitate to contact us at <strong><a href="mailto:info@zeinpieces.me" class="hover-secondary">info@zeinpieces.me</a></strong></p>
    </div>
    <?php get_template_part('partials/you-may-also-like/from-our-collections'); ?>
</div>

<?php
get_footer();
