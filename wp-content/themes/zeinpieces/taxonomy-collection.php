<?php
/**
 * The template for displaying Collection taxonomy
 *
 * @link    https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zeinpieces menu_order
 */
$collection = get_queried_object();

get_header();
?>

    <div class="category-menu">
        <div class="container">
            <?php show_the_breadcrumbs(); ?>
        </div>
    </div>

    <div class="main">
        <div class="container" style="margin-bottom: 30px;">
            <h2 class="kalimat-text"><?php echo $collection->name; ?></h2>
            <?php echo term_description( $collection->term_id, 'category' ); ?>
        </div>
        <div class="container">
            <div class="category-sec-main">
                <div class="select-category">
                    <div class="check-box-select">
                        <div class="filter-head">
                            <h4>Filter by</h4>
                        </div>
                        <?php echo do_shortcode("[wcpf_filters id='4336']") ?>
                    </div>
                    <div class="category-select">
                        <?php echo do_shortcode("[products class='collection,$collection->slug' columns='3' paginate='true' filter-id='4336']") ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
get_footer();
