<div class="slide-sec">
    <div class="left-side">
        <div class="your-class homepageSlider">
            <?php
            if( have_rows('slider_content') ):
                while( have_rows('slider_content') ) : the_row();
                    $image_url = get_sub_field('image');
                    $title = get_sub_field('title');
                    $subtitle = get_sub_field('subtitle');
                    $button_text = get_sub_field('button_text');
                    $button_link = get_sub_field('button_link'); ?>
                    <div class="slide-sec" style="margin: 0;">
                        <div class="section-slide">
                            <div class="left-slide">
                                <img src="<?php echo $image_url; ?>" alt="Image">
                            </div>
                            <div class="right-slide">
                                <div class="slide-text">
                                    <h1 class="slide-heading"><?php echo $title ?></h1>
                                    <p class="slide-sub-head"><?php echo $subtitle ?></p>
                                    <div class="slide-btn">
                                        <a href="<?php echo $button_link ?>" class="btn-text"><?php echo $button_text ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
               <?php endwhile;
            else :
                echo "Sorry No Posts";
            endif;
            ?>
        </div>
    </div>
</div>