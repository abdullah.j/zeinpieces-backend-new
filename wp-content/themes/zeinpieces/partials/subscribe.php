<div class="subscribe-sec">
    <div class="container">
        <h3 class="subscribe-head">WE'D LOVE TO SHARE OUR NEW COLLECTIONS, LATEST NEWS AND MUCH MORE WITH YOU!</h3>
        <h1 class="main-heading-subscribe">SUBSCRIBE TO OUR NEWSLETTER</h1>
    </div>
    <div class="mail-sec container">
        <form action="<?php echo admin_url( 'admin-ajax.php' ); ?>" method="POST" id="newsletter-subscription-form">
            <div class="d-inline-block" style="margin-bottom: 14px;">
                <input type="hidden" name="action" value="zp_newsletter">
                <input type="hidden" name="zp_form_nonce" value="<?php echo wp_create_nonce('zp-nonce'); ?>" />
                <input type="email" name="email" class="emai-btn" placeholder="Email address" required>

                <input type="submit" value="SUBMIT" class="submit-btn" />
            </div>
            <div class="g-recaptcha" data-sitekey="6LeIpJAUAAAAAIuQY7Jb26POhNVNZ8y0MUdYDPcr"></div>
        </form>
    </div>
</div>