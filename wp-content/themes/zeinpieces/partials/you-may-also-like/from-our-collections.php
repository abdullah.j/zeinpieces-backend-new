<?php
$collection = get_queried_object();

$args = array(
    'post_type'      => 'product',
    'posts_per_page' => 4,
    'fields'         => 'ids',
    'orderby'        => 'rand'
);
$products_query = new WP_Query( $args );
$products       = $products_query->get_posts();
?>

<div class="best-sec">
    <div class="container">
        <div class="phy-gift-heading">
            <h1 class="gift-recomended-sec">Best sellers</h1>
        </div>
        <div class="category-sec">
            <?php foreach ( $products as $post_id ) { ?>
                <?php the_post_block_four( $post_id );?>
            <?php } ?>
        </div>
    </div>
</div>