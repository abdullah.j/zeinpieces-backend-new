<?php
global $product;

$terms = get_the_terms($product->get_id(), 'collection');

$related_posts_args = array(
	'post_type'      => 'product',
	'posts_per_page' => 4,
	'fields'         => 'ids',
	'post__not_in'   => array( $post_id ),
	'orderby'        => 'rand',
);
$related_posts = get_posts( $related_posts_args );

?>
<div class="best-sec">
    <div class="container">
        <div class="phy-gift-heading">
            <h1 class="gift-recomended-sec">you may also like</h1>
        </div>
        <div class="category-sec">
            <?php foreach ( $related_posts as $post_id ) { ?>
                <?php the_post_block_four( $post_id );?>
            <?php } ?>
        </div>
    </div>
</div>