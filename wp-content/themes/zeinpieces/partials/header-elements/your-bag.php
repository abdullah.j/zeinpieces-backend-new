<?php
$items = WC()->cart->get_cart();
?>
<div class="header-action white-bg closed" id="header-your-bag">
    <div class="container">
        <a href="javascript:;" class="close-header-action hover-secondary">x</a>
        <div class="products-yourbag">
            <div class="yourBag">
                <h4>your bag</h4>

                <div class="slider-container" id="your-bag-header-slider-container">
                    <div class="slider" id="your-bag-header-slider">
                        <?php
                        foreach($items as $item => $values) {
                            $_product = $values['data'];
                            $product_id = apply_filters( 'woocommerce_cart_item_product_id', $values['product_id'], $values, $item );
                            ?>
                            <div class="slide slide-hidden">
                                <?php
                                echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                    '<a href="%s" class="remove remove-nitems" aria-label="%s" data-product_id="%s" data-product_sku="%s">x</a>',
                                    esc_url( wc_get_cart_remove_url( $item ) ),
                                    __( 'Remove this item', 'woocommerce' ),
                                    esc_attr( $product_id ),
                                    esc_attr( $_product->get_sku() )
                                ), $cart_item_key );
                                ?>
                                <a href="<?php echo $_product->get_permalink(); ?>" class="d-block hover-opacity piece-image">
                                    <?php echo wp_get_attachment_image($_product->get_image_id(), 'thumbnail') ?>
                                </a>
                                <a href="<?php echo $_product->get_permalink(); ?>" class="d-block hover-secondary piece-name">
                                    <?php echo $_product->get_name(); ?>

                                    (<?php echo $values['quantity'] ?>)
                                </a>
                                <div class="price">
                                    <?php echo $_product->get_price() . ' ' . get_woocommerce_currency(); ?>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                    <a href="javascript:" class="slider-arrow slider-arrow-left hover-secondary"><i class="fa fa-arrow-left"></i></a>
                    <a href="javascript:" class="slider-arrow slider-arrow-right hover-secondary"><i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
            <div class="yourOrder">
                <div class="order-summary">
                    <h4>ORDER SUMMARY</h4>
                    <div class="number-of-items clearfix">
                        <span>Number of items</span><span><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                    </div>
                    <div class="subtotal clearfix">
                        <span>SUBTOTAL</span><span><?php echo WC()->cart->get_cart_contents_total() . ' ' . get_woocommerce_currency() ?></span>
                    </div>

                    <div>
                        <a href="<?php echo wc_get_cart_url() ?>" class="zp-btn black-fill">view cart</a>
                    </div>

                    <div>
                        <a href="<?php echo wc_get_checkout_url() ?>" class="zp-btn black-fill">proceed to checkout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>