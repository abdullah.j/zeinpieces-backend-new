<div class="header-action closed" id="header-search-action">
    <div class="container">
        <a href="javascript:;" class="close-header-action hover-secondary">x</a>
        <form action="<?php echo home_url( '/' ); ?>" id="header-search-form">
            <input type="text" name="s" placeholder="Search for pieces and hit enter">

            <input type="hidden" value="product" name="post_type" id="post_type" />

        </form>
    </div>
</div>