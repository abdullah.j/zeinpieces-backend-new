<div class="header-action closed" id="header-action-account">
    <div class="container">
        <a href="javascript:;" class="close-header-action hover-secondary">x</a>
        <div class="contentWrapper">
            <div class="register">
                <h4>Register NOW</h4>
                <p>Don’t have an account? Register now!</p>
                <a href="<?php echo get_permalink( get_page_by_path( 'account' ) ); ?>" class="zp-btn black-fill">register now</a>
            </div>
            <div class="login">
                <h4>Log IN</h4>
                <p>Already registered? Please enter your email and password below.</p>

                <form action="?" id="login-form">
                    <p class="status"></p>
                    <div class="zp-form-row">
                        <div class="inline-field-container"><input type="email" id="username" name="email" placeholder="email" required></div>
                        <div class="inline-field-container"><input type="password" id="password" name="password" placeholder="password" required></div>
                    </div>
                    <div class="zp-form-row">
                        <input type="submit" value="login" class="zp-btn black-fill">
                    </div>
                    <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
                </form>

                <div class="text-left">
                    <a href="<?php echo wc_lostpassword_url(); ?>" class="forgot-password hover-secondary">Forgot my password</a>
                </div>
            </div>
        </div>
    </div>
</div>