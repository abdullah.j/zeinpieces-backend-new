<?php

/**

 * The template for displaying the footer

 *

 * Contains the closing of the #content div and all content after.

 *

 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials

 *

 * @package zeinpieces

 */



$theme_setting = get_option( 'theme_setting_option' );

?>

</div><!-- #wrapper -->

<style>

    /* Smartphones ----------- */

    @media only screen and (max-width: 760px) {

        .wup-desktop { display: none; }

    }

    @media only screen and (min-width: 761px) {

        .wup-mobile { display: none; }

    }

</style>

<footer>
    <?php
    if(is_page('personalize') || is_page('gift-card')){ ?>
    <div class="footer-section">
    <?php }else{ ?>
    <div class="footer-sec">
    <?php } ?>
        <div class="container">
            <div class="footer-menu">
                <?php wp_nav_menu( array( 'theme_location' => 'footer-menu-top', 'menu_class' => 'main-menu-footer-2', 'container'=>false ) ); ?>
                <?php wp_nav_menu( array( 'theme_location' => 'footer-menu-bottom', 'menu_class' => 'main-menu-footer', 'container'=>false ) ); ?>
            </div>
            <div class="icn-footer-img">
                <?php if( ! empty( $theme_setting['sm_instagram'] ) ) { ?>
                    <a target="_blank" href="<?php echo $theme_setting['sm_instagram'];?>"><img src="<?php echo THEME_URL; ?>/assets/images/i-5.png"></a>
                <?php } ?>
                <?php if( ! empty( $theme_setting['sm_facebook'] ) ) { ?>
                    <a target="_blank" href="<?php echo $theme_setting['sm_facebook'];?>"><img src="<?php echo THEME_URL; ?>/assets/images/i-6.png"></a>
                <?php } ?>
                <?php if( ! empty( $theme_setting['sm_youtube'] ) ) { ?>
                    <a target="_blank" href="<?php echo $theme_setting['sm_youtube'];?>"><img src="<?php echo THEME_URL; ?>/assets/images/i-7.png"></a>
                <?php } ?>
                <?php if( ! empty( $theme_setting['sm_pinterest'] ) ) { ?>
                    <a target="_blank" href="<?php echo $theme_setting['sm_pinterest'];?>"><img src="<?php echo THEME_URL; ?>/assets/images/i-8.png"></a>
                <?php } ?>
                <?php if( ! empty( $theme_setting['sm_tiktok'] ) ) { ?>
                    <a target="_blank" href="<?php echo $theme_setting['sm_tiktok'];?>"><img src="<?php echo THEME_URL; ?>/assets/images/i-9.png"></a>
                <?php } ?>
            </div>


        </div>
    </div>
</footer>

<?php echo $theme_setting['footer_script'];?>

<div class="ccw_plugin chatbot" style="bottom: 10px; right: 10px; display: inline; position:fixed; z-index:99999;">

    <!-- style 3  logo -->

    <div class="ccw_style3 animated no-animation ccw-no-hover-an ">

        <a target="_blank" href="https://wa.me/+96176678528/" class="img-icon-a wup-mobile">

            <img class="img-icon ccw-analytics" id="style-3" data-ccw="style-3" style="height: 64px;" src="https://zeinpieces.me/wp-content/uploads/2019/10/whatsapp-logo.png" alt="WhatsApp chat">

        </a>

        <a target="_blank" href="https://web.whatsapp.com/send?phone=0096176678528" class="img-icon-a wup-desktop">

            <img class="img-icon ccw-analytics" id="style-3" data-ccw="style-3" style="height: 64px;" src="https://zeinpieces.me/wp-content/uploads/2019/10/whatsapp-logo.png" alt="WhatsApp chat">

        </a>

    </div>

</div>

<?php wp_footer(); ?>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
</body>

</html>