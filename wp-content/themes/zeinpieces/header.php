<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package zeinpieces
 */

$theme_setting = get_option( 'theme_setting_option' );
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <title><?php wp_title(); ?></title>
    <link rel="manifest" href="<?php echo THEME_URL; ?>/assets/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo THEME_URL; ?>/assets/favicon/safari-pinned-tab.svg" color="#ecc3b2">
    <link rel="shortcut icon" href="<?php echo THEME_URL; ?>/assets/favicon/favicon.ico">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-config" content="<?php echo THEME_URL; ?>/assets/favicon/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    <script src='https://www.google.com/recaptcha/api.js'></script>
	<?php
	echo $theme_setting['header_script'];
	wp_head();
	?>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139522145-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-139522145-1');
    </script>
    <script type='text/javascript'>
        window.__lo_site_id = 157176;

        (function () {
            var wa = document.createElement('script');
            wa.type = 'text/javascript';
            wa.async = true;
            wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wa, s);
        })();
    </script>
    <!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1459347360866323');
  fbq('track', 'PageView');
</script>
<noscript>
  <img height="1" width="1" style="display:none"
       src="https://www.facebook.com/tr?id=1459347360866323&ev=PageView&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
<?php
global $woocommerce;
$checkoutTotal = $woocommerce->cart->total;
?>
<script type="text/javascript">
if(jQuery('body').hasClass('woocommerce-checkout')){
	jQuery(document).on('click', '#place_order', function(){
		fbq('track', 'Purchase', {value: <?= $checkoutTotal; ?>, currency: 'USD'});
	});
}
</script>
</head>
<body <?php body_class(); ?>>
<?php echo $theme_setting['body_script']; ?>
<header>
    <?php get_template_part( 'partials/header-elements/account-actions' ); ?>
    <?php get_template_part( 'partials/header-elements/header-search' ); ?>
    <?php get_template_part( 'partials/header-elements/your-bag' ); ?>
    <div class="container">
        <div class="header-sec">
            <div class="main-header">
                <div class="logo-menu">
                    <div class="menu-header">
                        <div class="menu-sec">
                            <?php
                            wp_nav_menu( array(
                                'theme_location'  => 'header-menu',
                                'container_class' => 'menu',
                                'menu_class'      => 'main-menu'
                            ) );
                            ?>
                        </div>
                    </div>
                    <div class="logo-header">
                        <a href="<?php echo home_url(); ?>">
                            <img src="<?php echo THEME_URL; ?>/assets/images/logo.png">
                        </a>
                    </div>
                </div>
            </div>
            <div class="main-header">
                <div class="icon-right">
                    <p class="right-text"><b>WELCOME ZEIN!</b> WE'RE SO HAPPY TO HAVE YOU.</p>
                    <div class="icn-img">
                        <ul>
                            <li>
                                <a href="javascript:;" class="hover-secondary header-action-toggle" data-action="header-action-account">
                                    <img src="<?php echo THEME_URL; ?>/assets/images/i-1.png">
                                </a>
                            </li>
                            <li>
                                <a href="javascript:;" class="hover-secondary header-action-toggle" data-action="header-search-action">
                                    <img src="<?php echo THEME_URL; ?>/assets/images/i-2.png">
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo get_permalink(get_page_by_path( 'wishlist' )) ?>">
                                    <img src="<?php echo THEME_URL; ?>/assets/images/i-3.png">
                                    <?php echo do_shortcode('[ti_wishlist_products_counter]'); ?>
                                </a>
                            </li>
                            <li class="bag-container">
                                <a href="#" class="hover-secondary header-action-toggle desktop" data-action="header-your-bag">
                                    <img src="<?php echo THEME_URL; ?>/assets/images/i-4.png">
                                </a>
                                <a href="<?php echo get_permalink( get_page_by_path( 'cart' ) ) ?>" class="hover-secondary mobile">
                                    <img src="<?php echo THEME_URL; ?>/assets/images/i-4.png">
                                </a>
                                <span id="bag-count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div id="wrapper">
