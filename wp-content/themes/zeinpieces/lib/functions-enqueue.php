<?php
add_action('wp_enqueue_scripts', 'enqueue_theme_scriptsnstyles');
function enqueue_theme_scriptsnstyles() {

    /*********** Styles ***********/

    wp_register_style('all-min', THEME_URL . '/assets/css/all.min.css' );
    wp_enqueue_style('all-min');

    wp_register_style('slick-theme', THEME_URL . '/assets/css/slick-theme.css' );
    wp_enqueue_style('slick-theme');

    wp_register_style('theme-style', THEME_URL . '/assets/css/style.css');
    wp_enqueue_style('theme-style');

    wp_register_style('media-queries', THEME_URL . '/assets/css/media.css' );
    wp_enqueue_style('media-queries');


 	/*********** Scripts ***********/

    wp_deregister_script('jquery');
    wp_register_script('jquery', THEME_URL . '/assets/js/jQuery.js', array(), '', TRUE);
    wp_enqueue_script('jquery');

    wp_register_script('all-min', THEME_URL . '/assets/js/all.min.js', array(), '', TRUE);
    wp_enqueue_script('all-min');

    wp_register_script('slick-theme', THEME_URL . '/assets/js/slick.min.js', array(), '', TRUE);
    wp_enqueue_script('slick-theme');

    wp_register_script('theme-main-js', THEME_URL . '/assets/js/custom.js', array(), '', TRUE);
    wp_enqueue_script('theme-main-js');


}