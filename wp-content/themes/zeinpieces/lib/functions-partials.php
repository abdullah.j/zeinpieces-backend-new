<?php

function the_post_block_one( $post_id ) {
    $link = get_permalink($post_id);
    if($post_id):
    ?>
    <div class="img-must-section">
        <a href="<?php echo $link; ?>">
            <?php echo get_the_post_thumbnail($post_id, 'thumbnail-551-415'); ?>
        </a>
        <div class="must-read-text">
            <p class="must-date"><?php echo get_the_date('M d.Y', $post_id); ?></p>
            <h3 class="ring-stacking"><?php echo get_the_title($post_id); ?></h3>
            <p class="info-text"><?php echo limit_text(apply_filters('the_excerpt', get_the_excerpt($post_id)), 22); ?></p>
            <div class="btn-view">
                <a href="<?php echo $link; ?>">View More >></a>
            </div>
        </div>
    </div>
    <?php
    endif;
}

function the_post_block_two( $post_id ) {
    $link = get_permalink($post_id);
    if($post_id):
    ?>
    <div class="img-jewelry-section">
        <?php echo get_the_post_thumbnail($post_id, 'thumbnail-361-272'); ?>
        <div class="must-read-text">
            <p class="must-date"><?php echo get_the_date('M d.Y', $post_id); ?></p>
            <h3 class="ring-stacking"><?php echo get_the_title($post_id); ?></h3>
            <p class="info-text"><?php echo limit_text(apply_filters('the_excerpt', get_the_excerpt($post_id)), 15); ?></p>
            <div class="btn-view">
                <a href="<?php echo $link; ?>">View More >></a>
            </div>
        </div>
    </div>
    <?php
    endif;
}

function the_post_block_three( $product_id ) {
    if($product_id):
        ?>
        <a href="<?php echo get_permalink( $product_id ); ?>">
            <div class="main-necklaces single-product-list">
                <div class="category-img-sec">
                    <?php echo get_the_post_thumbnail($product_id, 'thumbnail-361-272'); ?>
                    <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id='. $product_id .']') ?>
                </div>
                <div class="category-text-sec-2">
                    <h3><?php echo get_the_title($product_id); ?></h3>
                    <p><?php echo wc_get_product( $product_id )->get_price(); echo str_repeat('&nbsp;', 1); echo get_woocommerce_currency() ?></p>
                </div>
            </div>
        </a>
    <?php
    endif;
}

function the_post_block_four( $product_id = 36, $image_size = 'large',  $show_title = true, $show_price = false ){
    $product = wc_get_product($product_id);
    $attachment_id = get_field('collection_image', $product_id);
    ?>
    <a href="<?php echo get_permalink( $product_id ); ?>">
        <div class="main_cate">
            <div class="category-img-sec">
                <?php if (is_front_page() && $attachment_id['url'] != '') { ?>
                    <img src="<?php echo( $attachment_id['url'] ); ?>">
                    <?php
                } else {
                    echo get_the_post_thumbnail($product_id, $image_size);
                }
                ?>
                <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id='. $product_id .']') ?>
            </div>
            <div class="category-text">
                <?php if ($show_title === true) { ?>
                    <h3><?php echo $product->get_name(); ?></h3>
                <?php } ?>
                <?php if ($show_price === true || is_front_page()) { ?>
                    <p><?php echo $product->get_price_html(); ?></p>
                <?php } ?>

            </div>
        </div>
    </a>
<?php }
