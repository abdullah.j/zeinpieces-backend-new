<?php

// define the woocommerce_after_single_product callback
function action_woocommerce_after_single_product(  ) {
	// make action magic happen here...
	get_template_part('partials/you-may-also-like/you-may-also-like');
};

// add the action
add_action( 'woocommerce_after_single_product', 'action_woocommerce_after_single_product', 10, 0 );


remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

add_filter( 'woocommerce_dropdown_variation_attribute_options_html', 'filter_dropdown_option_html', 12, 2 );
function filter_dropdown_option_html( $html, $args ) {
    $show_option_none_text = $args['show_option_none'] ? $args['show_option_none'] : __( 'Choose an option', 'woocommerce' );
    $show_option_none_html = '<option value="">' . esc_html( $show_option_none_text ) . '</option>';

    $html = str_replace($show_option_none_html, '', $html);

    return $html;
}


add_action( 'woocommerce_before_add_to_cart_quantity', 'wp_echo_qty_front_add_cart' );

function wp_echo_qty_front_add_cart() {
    echo '<div class="qty">Quantity: </div>';
   // do_action( 'woocommerce_before_add_to_cart_quantity' );
}