<?php
function woocom_extra_register_fields() {?>

    <p class="form-row form-row-wide">

        <label for="reg_billing_first_name"><?php _e( 'First name', 'woocommerce' ); ?><span class="required">*</span></label><input type="text" class="input-text" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" /></p>

    <p class="form-row form-row-wide">

        <label for="reg_billing_last_name"><?php _e( 'Last name', 'woocommerce' ); ?><span class="required">*</span></label><input type="text" class="input-text" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" /></p>

    <?php

}

add_action( 'woocommerce_register_form_start', 'woocom_extra_register_fields' );

function registration_errors_validation($reg_errors, $sanitized_user_login, $user_email) {
    global $woocommerce;
    extract( $_POST );
    if ( strcmp( $password, $password2 ) !== 0 ) {
        return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
    }
    return $reg_errors;
}
add_filter('woocommerce_registration_errors', 'registration_errors_validation', 10,3);

function woocommerce_register_form_password_repeat() {
    ?>
    <p class="form-row form-row-wide">
        <label for="reg_password2"><?php _e( 'Confirm password', 'woocommerce' ); ?> <span class="required">*</span></label>
        <input type="password" class="input-text" name="password2" id="reg_password2" value="<?php if ( ! empty( $_POST['password2'] ) ) echo esc_attr( $_POST['password2'] ); ?>" />
    </p>
    <?php
}

add_action( 'woocommerce_register_form', 'woocommerce_register_form_password_repeat' );


function woocom_validate_extra_register_fields( $username, $email, $validation_errors )

{

    if (isset($_POST['billing_first_name']) && empty($_POST['billing_first_name']) ) {

        $validation_errors->add('billing_first_name_error', __('First Name is required!', 'woocommerce'));

}

    if (isset($_POST['billing_last_name']) && empty($_POST['billing_last_name']) ) {

        $validation_errors->add('billing_last_name_error', __('Last Name is required!', 'woocommerce'));

}

    return $validation_errors;

}

add_action('woocommerce_register_post', 'woocom_validate_extra_register_fields', 10, 3);

function woocom_save_extra_register_fields($customer_id) {

    if (isset($_POST['billing_first_name'])) {

        update_user_meta($customer_id, 'billing_first_name', sanitize_text_field($_POST['billing_first_name']));

    }

    if (isset($_POST['billing_last_name'])) {

        update_user_meta($customer_id, 'billing_last_name', sanitize_text_field($_POST['billing_last_name']));

    }

}

add_action('woocommerce_created_customer', 'woocom_save_extra_register_fields');

function text_domain_woo_save_reg_form_fields($customer_id) {
    //First name field
    if (isset($_POST['billing_first_name'])) {
        update_user_meta($customer_id, 'first_name', sanitize_text_field($_POST['billing_first_name']));
        update_user_meta($customer_id, 'billing_first_name', sanitize_text_field($_POST['billing_first_name']));
    }
    //Last name field
    if (isset($_POST['billing_last_name'])) {
        update_user_meta($customer_id, 'last_name', sanitize_text_field($_POST['billing_last_name']));
        update_user_meta($customer_id, 'billing_last_name', sanitize_text_field($_POST['billing_last_name']));
    }
}

add_action('woocommerce_created_customer', 'text_domain_woo_save_reg_form_fields');



function change_lost_your_password ($text) {

    if ($text == 'Lost your password? Please enter your username or email address. You will receive a link to create a new password via email.'){
        $text = 'PLEASE ENTER YOUR EMAIL ADDRESS';

    }

    if ($text == 'Proceed to Areeba'){
        $text = 'Continue Transaction';

    }

    if ($text == 'Enter amount (Only digits)'){
        $text = 'Enter amount';

    }

    return $text;
}
add_filter( 'gettext', 'change_lost_your_password' );