<?php
function customized_pieces() {


    //echo get_template_directory_uri();
   // var_dump($_FILES);
    if (isset($_POST["email-address"]) && wp_verify_nonce($_POST['book_customize_nonce'], 'zeinpieces-book-customize')) {

         global $wpdb;

           $table_booking_user = $wpdb->prefix . 'zp_contact_us';

           $book_appointment = $wpdb->insert(
               $table_booking_user,
               array('date'  => date('Y-m-d H:i:s'),
                   'subject'  => $_POST["subject"],
                   'name'    => $_POST["first-name"]." " .$_POST["last-name"] ,
                   'email'   => $_POST["email-address"],
                   'number'  => $_POST["telephone"],
                   'message' => $_POST["message"],
                   'type' => 'Personalize',
                   'country'  => $_POST["country"]
               )
           );


        if(!empty($_FILES["attachments"]["name"])){
            $file_file = $_FILES['attachments'];
            $image = $file_file['name'];
            $extension = strtolower(strrchr($image, '.'));
            $imageName = uniqid().$extension;
            $upload_dir = wp_upload_dir();
            $imagepath = $upload_dir['basedir']."/".$imageName;
            $imagepathurl = $upload_dir['baseurl']."/".$imageName;
            $_SESSION['personalizeImagePath'] = $imagepathurl;
            move_uploaded_file($file_file['tmp_name'],$imagepath);
            //$update_families =$db->update_sql ( "update $table_booking_user set image='".$imageName."' where id='".$id."' ");
            //$wpdb->update($table_booking_user, array('file'=>'parvez', array('id' => $book_appointment)));

            //$wpdb->update($table_booking_user, array('file'=>"parvez"), array( "id", $book_appointment));
        }


         if (!$book_appointment) {
            wp_send_json(array('error' => array('message' => 'Sorry, there is an error Sending a Request!')));
        }



        // send email
        if ( $book_appointment && ! is_wp_error( $book_appointment ) ) {
        //if(1){


           $email_path = WP_PLUGIN_DIR .'/zp-forms/emails/request-customized-user.php';
           $user_content = getRenderedHTML($email_path, $_POST);
           $title = 'Customized Pieces Received | Zein Pieces';
           wp_mail($_POST["email-address"], $title, $user_content);

            $headers[] = 'Cc: orders@zeinpieces.me'; // note you can just use a simple email address

           $admin_email = 'info@zeinpieces.me';
           $admin_content = getRenderedHTML(WP_PLUGIN_DIR .'/zp-forms/emails/request-customized-admin.php', $_POST);
           wp_mail($admin_email, 'New Booking Request | Zein Pieces', $admin_content, $headers);
        }

        wp_send_json(array('success' => array('message' => 'Thank you for subscribing to our newsletter')));

    } else {
        wp_send_json(array( 'error' => array( 'message' => 'Sorry, there is an error in the submitted fields!')));
    }
}
add_action('wp_ajax_nopriv_customized_pieces', 'customized_pieces');
add_action('wp_ajax_customized_pieces', 'customized_pieces');

?>