<?php
/**
 * Template Name: Celebrities
 */

get_header();

$current_category_id = get_queried_object()->term_id;

$category_query = new WP_Query(array(
    'post_type'         => 'post',
    'post_status'       => 'publish',
    'cat'           => $current_category_id,
    'posts_per_page'    => -1,
    'fields'            => 'ids',
    'order'             => 'DESC',
));

$counter = 0; ?>

    <div class="category-menu">
        <div class="container">
            <?php show_the_breadcrumbs();?>
        </div>
    </div>
    <div class="main">
        <?php if ( $category_query->have_posts() ) :
            while ( $category_query->have_posts() ) : $category_query->the_post();
                $post_id = get_the_ID();
                $link = get_permalink($post_id);
                $counter = $counter +1;
                $initial_counter = ($counter == 1) ? "<div class='main-must-gallery'><div class='container'><div class='must-gallery-sec'>" : '';
                $end_counter = ($counter <= 3) ? $end_value = (($category_query->current_post +1) == ($category_query->post_count) || $counter == 3) ? '</div></div></div>' : '' : '';
                echo $initial_counter; ?>
                <div class="img-jewelry-section">
                    <?php echo get_the_post_thumbnail($post_id, 'thumbnail-361-272'); ?>
                    <div class="must-read-text">
                        <p class="must-date"><?php echo get_the_date('M d.Y', $post_id); ?></p>
                        <h3 class="ring-stacking"><?php echo get_the_title($post_id); ?></h3>
                        <p class="info-text"><?php echo limit_text(apply_filters('the_excerpt', get_the_excerpt($post_id)), 15); ?></p>
                        <div class="btn-view">
                            <a href="<?php echo $link; ?>">View More >></a>
                        </div>
                    </div>
                </div>
                <?php
                echo $end_counter;
                $reset_counter = ($counter == 3) ? $counter = 0 : '';
            endwhile; wp_reset_postdata(); else : ?>

            <p><?php _e( 'Sorry, no Posts' ); ?></p>

        <?php endif; ?>
        <?php echo get_template_part('partials/subscribe') ?>

    </div>
<?php
get_footer();