<?php
/**
 * Template Name: Contact
 */

$phone_number_one = get_field( 'phone_number_one' );
$phone_number_two = get_field( 'phone_number_two' );
$email_address = get_field( 'email_address' );
$link_one = get_field( 'link_one' );
$title_one = get_field( 'title_one' );
$description_one = get_field( 'description_one' );
$link_two = get_field( 'link_two' );
$title_two = get_field( 'title_two' );
$description_two = get_field( 'description_two' );

get_header(); ?>

    <div class="category-menu">
        <div class="container">
            <?php show_the_breadcrumbs();?>
        </div>
    </div>
    <div class="main">
        <div class="container">
            <div class="header-2-personalize">
                <h1>
                    get in touch
                </h1>
            </div>
            <div class="sec-contact-us-main">
                <div class="container">
                    <div class="row-contact-us-sec">
                        <div class="col-contact-us-sec">
                            <div class="col-whatsapp-call-sec">
                                <div class="row-whatsapp">
                                    <h1>whatsapp or call</h1>
                                    <div class="col-whatsapp-call">
                                        <div class="col-leb">
                                            <h1>leb</h1>
                                            <a href="tel:<?php echo $phone_number_one ?>"><?php echo $phone_number_one ?></a>
                                        </div>
                                        <div class="col-dxb">
                                            <h1>dxb</h1>
                                            <a href="tel:<?php echo $phone_number_two ?>"><?php echo $phone_number_two ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-email-add-sec">
                                <div class="row-email">
                                    <h1>email address</h1>
                                    <div class="col-email-add">
                                        <a href="mailto:<?php echo $email_address ?>"><?php echo $email_address ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form2">
                        <form action="<?php echo admin_url( 'admin-ajax.php' ); ?>" method="POST" id="contact-us-form">
                            <div class="col-heading3">
                                <h1>send an inquiry</h1>
                            </div>
                            <div class="col-form">
                                <div class="success-message-container"></div>
                            </div>
                            <div class="col-form">
                                <div class="col-fname relative">
                                    <input type="text" name="name" id="name" class="zp-form-field" required placeholder="Full Name*">
                                </div>
                                <div class="col-email relative">
                                    <input type="email" name="email" id="email" class="zp-form-field" required placeholder="Email*">
                                </div>
                            </div>
                            <div class="col-form">
                                <div class="col-number relative">
                                    <input type="text" name="number" id="phone-number" class="zp-form-field" placeholder="Phone Number">
                                </div>
                                <div class="col-subject relative">
                                    <input type="text" name="subject" id="subject" class="zp-form-field" required placeholder="Subject*">
                                </div>
                            </div>
                            <div class="col-form">
                                <div class="col-message relative">
                                    <input name="message" id="message" placeholder="Message*" class="zp-form-field" required />
                                </div>
                            </div>
                            <div class="g-recaptcha" data-sitekey="6LeIpJAUAAAAAIuQY7Jb26POhNVNZ8y0MUdYDPcr"></div>
                            <div class="col-form-3">
                                <div class="col-send">
                                    <input type="hidden" name="action" value="zp_contact_us">
                                    <input type="hidden" name="zp_form_nonce" value="<?php echo wp_create_nonce('zp-nonce'); ?>"/>
                                    <input type="submit" value="SEND" class="zp-btn black-fill full-width">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="row-find-sec">
                        <div class="col-find-sec">
                            <h1>find us at</h1>
                        </div>
                        <div class="col-find-contant">
                            <div class="row-find-lebanon">
                                <div class="col-find-lebanon">
                                    <iframe src="<?php echo $link_one ?>" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                                    <h1><?php echo $title_one ?></h1>
                                    <a><?php echo $description_one ?></a>
                                </div>
                            </div>
                            <div class="row-find-dubai">
                                <div class="col-find-dubai">
                                    <iframe src="<?php echo $link_one ?>" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                                    <h1><?php echo $title_two ?></h1>
                                    <a><?php echo $description_two ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo get_template_part('partials/subscribe') ?>
    </div>
<?php
get_footer();