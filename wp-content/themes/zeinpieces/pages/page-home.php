<?php
/*

 Template Name: Home page

 */
get_header();
$get_fields = get_fields();
$customize_section = $get_fields['customize_section'];
$products_section = $get_fields['products_section'];
$blog_posts_args = array(
    'post_type' => 'post',
    'posts_per_page' => 4
);
$blog_posts = new WP_Query($blog_posts_args);
$collection_index_start = 0;
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
<link href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css" rel="stylesheet"/>
<link href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick-theme.css" rel="stylesheet"/>
<?php get_template_part('partials/top-slider'); ?>
<div class="main">

    <?php if( have_rows('gender_section') ): ?>
            <?php while( have_rows('gender_section') ): the_row();
                $off = get_sub_field('turn_off_section')[0];
                $image_one = get_sub_field('image_one');
                $image_two = get_sub_field('image_two');
                $image_three = get_sub_field('image_three');
                $title_one = get_sub_field('title_one');
                $title_two = get_sub_field('title_two');
                $title_three = get_sub_field('title_three'); ?>
                <?php if($off != 'off'): ?>
                    <div class="gallery-sec">
                        <div class="main_img">
                            <a href="<?php echo get_permalink( get_page_by_path( 'women' ) ) ?>">
                                <h1 class="gallery-heading"><?php echo $title_one ?></h1>
                                <img src="<?php echo $image_one ?>" alt="Image">
                            </a>
                        </div>
                        <div class="main_img">
                            <a href="<?php echo get_permalink( get_page_by_path( 'men' ) ) ?>">
                                <h1 class="gallery-heading"><?php echo $title_two ?></h1>
                                <img src="<?php echo $image_two ?>" alt="Image">
                            </a>
                        </div>
                        <div class="main_img">
                            <a href="<?php echo get_permalink( get_page_by_path( 'kids' ) ) ?>">
                                <h1 class="gallery-heading"><?php echo $title_three ?></h1>
                                <img src="<?php echo $image_three ?>" alt="Image">
                            </a>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>

    <?php if( have_rows('just_landed_section') ): ?>
        <?php while( have_rows('just_landed_section') ): the_row();
            $title = get_sub_field('title');
            $products = get_sub_field('just_landed_products');
            $off = get_sub_field('turn_off_section')[0];
            $product_count = 0; ?>
            <?php if($off != 'off'): ?>
                <div class="container">
                <div class="category-sec display-inline-block">
                    <h1 class="main-heding-category" style="margin-top: 0;"><?php echo $title; ?></h1>
                    <?php foreach($products as $product){
                        $product_count++;
                        if($product_count <= 4){ ?>
                            <div class="main_cate">
                                <div class="category-img-sec">
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url($product->ID,'thumbnail-255-260') ?>" alt="Image">
                                        <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id='. $product->ID .']') ?>
                                    </a>
                                </div>
                                <div class="category-text">
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <h3><?php echo $product->post_title; ?></h3>
                                        <p><?php echo wc_get_product( $product->ID )->get_price(); echo str_repeat('&nbsp;', 1); echo get_woocommerce_currency() ?></p>
                                    </a>
                                </div>
                            </div>
                       <?php
                        }
                     } ?>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('say_my_name_section') ): ?>
        <?php while( have_rows('say_my_name_section') ): the_row();
            $image = get_sub_field('image');
            $title = get_sub_field('title');
            $subtitle = get_sub_field('subtitle');
            $button_text = get_sub_field('button_text');
            $button_link = get_sub_field('button_link');
            $products = get_sub_field('say_my_name_products');
            $off = get_sub_field('turn_off_section')[0];
            $reverse = get_sub_field('reverse_image_position')[0];
            $product_count = 0;?>
            <?php if($off != 'off'): ?>
                <div class="container">
                    <div class="name-sec">
                        <?php if($reverse == 'reverse'): ?>
                            <div class="right-name">
                                <img src="<?php echo $image; ?>" width="100%" height="500px">
                            </div>
                            <div class="left-name">
                                <div class="show-text">
                                    <h1 class="show-heading"><?php echo $title; ?></h1>
                                    <p class="show-sub-head"><?php echo $subtitle; ?></p>
                                    <div class="slide-btn2">
                                        <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                    </div>
                                </div>
                            </div>
                        <?php endif ?>
                        <?php if($reverse != 'reverse'): ?>
                            <div class="left-name">
                                <div class="show-text">
                                    <h1 class="show-heading"><?php echo $title; ?></h1>
                                    <p class="show-sub-head"><?php echo $subtitle; ?></p>
                                    <div class="slide-btn2">
                                        <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="right-name">
                            <img src="<?php echo $image; ?>" width="100%" height="500px">
                        </div>
                        <?php endif ?>
                    </div>
                </div>
                <div class="container">
                <div class="category-sec">
                    <?php foreach($products as $product){
                    $product_count++;
                        if($product_count <= 4){ ?>
                            <div class="main_cate1">
                                <div class="category-img-sec">
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url($product->ID,'thumbnail-255-260') ?>" alt="Image">
                                        <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id='. $product->ID .']') ?>
                                    </a>
                                </div>
                                <div class="category-text">
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <h3><?php echo $product->post_title; ?></h3>
                                        <p><?php echo wc_get_product( $product->ID )->get_price(); echo str_repeat('&nbsp;', 1); echo get_woocommerce_currency() ?></p>
                                    </a>
                                </div>
                            </div>
                        <?php
                        }
                    } ?>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('middle_three_section_one') ): ?>
        <?php while( have_rows('middle_three_section_one') ): the_row();
            $image = get_sub_field('image');
            $title = get_sub_field('title');
            $button_text = get_sub_field('button_text');
            $button_link = get_sub_field('button_link');
            $reverse = get_sub_field('reverse_image_position')[0];
            $off = get_sub_field('turn_off_section')[0]; ?>
            <?php if($off != 'off'): ?>
                <div class="container">
                    <div class="name-sec">
                        <?php if($reverse != 'reverse'): ?>
                            <div class="right-name">
                                <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                            </div>
                            <div class="left-name">
                                <div class="show-text">
                                    <h1 class="show-heading"><?php echo $title; ?></h1>
                                    <div class="slide-btn2">
                                        <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if($reverse == 'reverse'): ?>
                            <div class="left-name">
                                <div class="show-text">
                                    <h1 class="show-heading"><?php echo $title; ?></h1>
                                    <div class="slide-btn2">
                                        <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="right-name">
                                <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('middle_three_section_two') ): ?>
        <?php while( have_rows('middle_three_section_two') ): the_row();
            $image = get_sub_field('image');
            $title = get_sub_field('title');
            $button_text = get_sub_field('button_text');
            $button_link = get_sub_field('button_link');
            $reverse = get_sub_field('reverse_image_position')[0];
            $off = get_sub_field('turn_off_section')[0]; ?>
            <?php if($off != 'off'): ?>
                <div class="container">
                <div class="name-sec">
                    <?php if($reverse != 'reverse'): ?>
                        <div class="left-name">
                            <div class="show-text">
                                <h1 class="show-heading"><?php echo $title; ?></h1>
                                <div class="slide-btn2">
                                    <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                </div>
                            </div>
                        </div>
                        <div class="right-name">
                            <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                        </div>
                    <?php endif; ?>
                    <?php if($reverse == 'reverse'): ?>
                        <div class="right-name">
                            <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                        </div>
                        <div class="left-name">
                            <div class="show-text">
                                <h1 class="show-heading"><?php echo $title; ?></h1>
                                <div class="slide-btn2">
                                    <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('middle_three_section_three') ): ?>
        <?php while( have_rows('middle_three_section_three') ): the_row();
            $image = get_sub_field('image');
            $title = get_sub_field('title');
            $button_text = get_sub_field('button_text');
            $button_link = get_sub_field('button_link');
            $reverse = get_sub_field('reverse_image_position')[0];
            $off = get_sub_field('turn_off_section')[0]; ?>
            <?php if($off != 'off'): ?>
                <div class="container">
                <div class="name-sec">
                    <?php if($reverse != 'reverse'): ?>
                        <div class="right-name">
                            <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                        </div>
                        <div class="left-name">
                            <div class="show-text">
                                <h1 class="show-heading"><?php echo $title; ?></h1>
                                <div class="slide-btn2">
                                    <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($reverse == 'reverse'): ?>
                        <div class="left-name">
                            <div class="show-text">
                                <h1 class="show-heading"><?php echo $title; ?></h1>
                                <div class="slide-btn2">
                                    <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                </div>
                            </div>
                        </div>
                        <div class="right-name">
                            <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('middle_three_products_section') ): ?>
        <?php while( have_rows('middle_three_products_section') ): the_row();
            $products = get_sub_field('middle_three_section_products');
            $product_count = 0;
            $off = get_sub_field('turn_off_section')[0]; ?>
            <?php if($off != 'off'): ?>
                <div class="container">
                <div class="category-sec">
                    <?php foreach($products as $product){
                        $product_count++;
                        if($product_count <= 4){ ?>
                            <div class="main_cate1">
                                <div class="category-img-sec">
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url($product->ID,'thumbnail-255-260') ?>" alt="Image">
                                        <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id='. $product->ID .']') ?>
                                    </a>
                                </div>
                                <div class="category-text">
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <h3><?php echo $product->post_title; ?></h3>
                                        <p><?php echo wc_get_product( $product->ID )->get_price(); echo str_repeat('&nbsp;', 1); echo get_woocommerce_currency() ?></p>
                                    </a>
                                </div>
                            </div>
                            <?php
                        }
                    } ?>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('best_sellers_section') ): ?>
        <?php while( have_rows('best_sellers_section') ): the_row();
            $title = get_sub_field('title');
            $products = get_sub_field('best_sellers_products');
            $product_count = 0;
            $off = get_sub_field('turn_off_section')[0]; ?>
            <?php if($off != 'off'): ?>
                <div class="best-seller-backgroung">
                <div class="container">
                    <div class="category-sec display-inline-block">
                        <h1 class="main-heding-category"><?php echo $title; ?></h1>
                        <?php foreach($products as $product){
                            $product_count++;
                            if($product_count <= 4){ ?>
                                <div class="main_cate">
                                    <div class="category-img-sec">
                                        <a href="<?php echo get_permalink( $product->ID ); ?>">
                                            <img src="<?php echo get_the_post_thumbnail_url($product->ID,'thumbnail-255-260') ?>" alt="Image">
                                            <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id='. $product->ID .']') ?>
                                        </a>
                                    </div>
                                    <div class="category-text">
                                        <a href="<?php echo get_permalink( $product->ID ); ?>">
                                            <h3><?php echo $product->post_title; ?></h3>
                                            <p><?php echo wc_get_product( $product->ID )->get_price(); echo str_repeat('&nbsp;', 1); echo get_woocommerce_currency() ?></p>
                                        </a>
                                    </div>
                                </div>
                                <?php
                            }
                        } ?>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('beauty_section') ): ?>
        <?php while( have_rows('beauty_section') ): the_row();
            $title = get_sub_field('title');
            $subtitle = get_sub_field('subtitle');
            $products = get_sub_field('beauty_products');
            $product_count = 0;
            $off = get_sub_field('turn_off_section')[0]; ?>
            <?php if($off != 'off'): ?>
                <div class="container">
                <div class="category-sec display-inline-block">
                    <h1 class="beauti-heading"><?php echo $title; ?></h1>
                    <p class="sub-text-category"><?php echo $subtitle; ?></p>
                    <?php foreach($products as $product){
                        $product_count++;
                        if($product_count <= 4){ ?>
                            <div class="main_cate">
                                <div class="category-img-sec">
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url($product->ID,'thumbnail-255-260') ?>" alt="Image">
                                        <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id='. $product->ID .']') ?>
                                    </a>
                                </div>
                                <div class="category-text">
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <h3><?php echo $product->post_title; ?></h3>
                                        <p><?php echo wc_get_product( $product->ID )->get_price(); echo str_repeat('&nbsp;', 1); echo get_woocommerce_currency() ?></p>
                                    </a>
                                </div>
                            </div>
                            <?php
                        }
                    } ?>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('highlights_section') ): ?>
        <?php while( have_rows('highlights_section') ): the_row();
            $title = get_sub_field('title');
            $first_box_image = wp_get_attachment_image_src( get_sub_field('first_box_image'), 'thumbnail-349-450' )[0];
            $first_box_text = get_sub_field('first_box_text');
            $second_box_image = wp_get_attachment_image_src( get_sub_field('second_box_image'), 'thumbnail-349-450' )[0];
            $second_box_text = get_sub_field('second_box_text');
            $third_box_image = wp_get_attachment_image_src( get_sub_field('third_box_image'), 'thumbnail-349-450' )[0];
            $third_box_text = get_sub_field('third_box_text');
            $off = get_sub_field('turn_off_section')[0];
             ?>
            <?php if($off != 'off'): ?>
                <div class="container">
                <div class="gallery-sec-1">

                    <h1 class="heigh-heading"><?php echo $title; ?></h1>

                    <div class="main-heighlight">
                        <div class="category-img-sec">
                            <img src="<?php echo $first_box_image ?>">
                        </div>
                        <div class="heighlight_text">
                            <h4>Our Inspiration: Get to know more about<br> how we work.</h4>
                        </div>
                    </div>

                    <div class="main-heighlight">
                        <div class="category-img-sec">
                            <img src="<?php echo $second_box_image ?>">
                        </div>
                        <div class="heighlight_text">
                            <h4>Jewelry Storing: Tips and tricks on how to<br> store your pieces to keep them protected.</h4>
                        </div>
                    </div>

                    <div class="main-heighlight">
                        <div class="category-img-sec">
                            <img src="<?php echo $third_box_image ?>">
                        </div>
                        <div class="heighlight_text">
                            <h4>Melissa Mouzannar rocking our Double<br> Sided Kalima Tag Necklace</h4>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('man_personalize_section') ): ?>
        <?php while( have_rows('man_personalize_section') ): the_row();
            $image = get_sub_field('image');
            $title = get_sub_field('title');
            $button_text = get_sub_field('button_text');
            $button_link = get_sub_field('button_link');
            $products = get_sub_field('name_personalize_products');
            $reverse = get_sub_field('reverse_image_position')[0];
            $off = get_sub_field('turn_off_section')[0];
            $product_count = 0;?>
            <?php if($off != 'off'): ?>
                <div class="container">
                    <div class="name-sec">
                        <?php if($reverse != 'reverse'): ?>
                            <div class="left-name">
                                <div class="show-text">
                                    <h1 class="show-heading"><?php echo $title; ?></h1>
                                    <div class="slide-btn2">
                                        <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                    </div>
                                </div>
                            </div>
                            <div class="right-name">
                                <img src="<?php echo $image; ?>" width="100%" height="500px">
                            </div>
                        <?php endif; ?>
                        <?php if($reverse == 'reverse'): ?>
                            <div class="right-name">
                                <img src="<?php echo $image; ?>" width="100%" height="500px">
                            </div>
                            <div class="left-name">
                                <div class="show-text">
                                    <h1 class="show-heading"><?php echo $title; ?></h1>
                                    <div class="slide-btn2">
                                        <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="container">
                <div class="category-sec">
                    <?php foreach($products as $product){
                        $product_count++;
                        if($product_count <= 4){ ?>
                            <div class="main_cate1">
                                <div class="category-img-sec">
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <img src="<?php echo get_the_post_thumbnail_url($product->ID,'thumbnail-255-260') ?>" alt="Image">
                                        <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id='. $product->ID .']') ?>
                                    </a>
                                </div>
                                <div class="category-text">
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <h3><?php echo $product->post_title; ?></h3>
                                        <p><?php echo wc_get_product( $product->ID )->get_price(); echo str_repeat('&nbsp;', 1); echo get_woocommerce_currency() ?></p>
                                    </a>
                                </div>
                            </div>
                            <?php
                        }
                    } ?>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('moments_section') ): ?>
        <?php while( have_rows('moments_section') ): the_row();
            $title = get_sub_field('title');
            $hash = get_sub_field('hash');
            $off = get_sub_field('turn_off_section')[0]; ?>
            <?php if($off != 'off'): ?>
                <div class="moments-sec">
                <div class="container">
                    <h1 class="moments-head"><?php echo $title ?></h1>
                    <h1 class="main-heading-moments"><?php echo $hash ?></h1>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('gift_cards_section') ): ?>
        <?php while( have_rows('gift_cards_section') ): the_row();
            $image = get_sub_field('image');
            $title = get_sub_field('title');
            $subtitle = get_sub_field('subtitle');
            $button_text = get_sub_field('button_text');
            $button_link = get_sub_field('button_link');
            $reverse = get_sub_field('reverse_image_position')[0];
            $off = get_sub_field('turn_off_section')[0]; ?>
            <?php if($off != 'off'): ?>
                <div class="container">
                <div class="name-sec">
                    <?php if($reverse != 'reverse'): ?>
                        <div class="right-name">
                            <img src="<?php echo $image ?>" width="100%" height="500px">
                        </div>
                        <div class="left-name">
                            <div class="show-text">
                                <p class="show-sub-head-1"><?php echo $subtitle ?></p>
                                <h1 class="show-heading"><?php echo $title ?></h1>
                                <div class="slide-btn2">
                                    <a href="<?php echo $button_link ?>" class="btn-text"><?php echo $button_text ?></a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if($reverse == 'reverse'): ?>
                        <div class="left-name">
                            <div class="show-text">
                                <p class="show-sub-head-1"><?php echo $subtitle ?></p>
                                <h1 class="show-heading"><?php echo $title ?></h1>
                                <div class="slide-btn2">
                                    <a href="<?php echo $button_link ?>" class="btn-text"><?php echo $button_text ?></a>
                                </div>
                            </div>
                        </div>
                        <div class="right-name">
                            <img src="<?php echo $image ?>" width="100%" height="500px">
                        </div>
                    <?php endif; ?>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('videos_section') ): ?>
        <?php while( have_rows('videos_section') ): the_row();
            $title = get_sub_field('title');
            $video_one_title = get_sub_field('video_one_title');
            $video_one_subtitle = get_sub_field('video_one_subtitle');
            $video_one = get_sub_field('video_one', false);
            $video_thumbnail_one = get_sub_field('video_thumbnail_one');
            $video_two_title = get_sub_field('video_two_title');
            $video_two_subtitle = get_sub_field('video_two_subtitle');
            $video_two = get_sub_field('video_two', false);
            $video_thumbnail_two = get_sub_field('video_thumbnail_two');
            $video_three_title = get_sub_field('video_three_title');
            $video_three_subtitle = get_sub_field('video_three_subtitle');
            $video_three = get_sub_field('video_three', false);
            $all_videos_link = get_sub_field('all_videos_link');
            $video_thumbnail_three = get_sub_field('video_thumbnail_three');
            $off = get_sub_field('turn_off_section')[0]; ?>
            <?php if($off != 'off'): ?>
                <div class="container">
                    <h1 class="video-head"><?php echo $title; ?></h1>
                    <div id="London" class="tabcontent">
                        <div class="special-img">
                            <img src="<?php echo wp_get_attachment_image_src($video_thumbnail_one,'thumbnail-1108-601')[0]; ?>">
                            <a data-video-link="<?php echo $video_one; ?>" href="#" class="icon-play"><i class="fas fa-play"></i></a>
                        </div>

                    </div>
                    <div id="Paris" class="tabcontent">
                        <div class="special-img">
                            <img src="<?php echo wp_get_attachment_image_src($video_thumbnail_two,'thumbnail-1108-601')[0]; ?>">
                            <a data-video-link="<?php echo $video_two; ?>" href="#" class="icon-play"><i class="fas fa-play"></i></a>
                        </div>
                    </div>
                    <div id="Tokyo" class="tabcontent">
                        <div class="special-img">
                            <img src="<?php echo wp_get_attachment_image_src($video_thumbnail_three,'thumbnail-1108-601')[0]; ?>">
                            <a data-video-link="<?php echo $video_three; ?>" href="#" class="icon-play"><i class="fas fa-play"></i></a>
                        </div>
                    </div>
                    <div class="tab">
                        <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">
                            <div class="special-img-btn">
                                <img src="<?php echo wp_get_attachment_image_src($video_thumbnail_one,'thumbnail-347-230')[0]; ?>" width="100%">
                                <p class="Valentine-special"><b><?php echo $video_one_title ?> |</b><i> <?php echo $video_one_subtitle ?></i></p>
                                <a data-video-link="<?php echo $video_one; ?>" href="#" class="icon-play-btn"><i class="fas fa-play"></i></a>
                            </div>
                        </button>
                        <button class="tablinks" onclick="openCity(event, 'Paris')">
                            <div class="special-img-btn">
                                <img src="<?php echo wp_get_attachment_image_src($video_thumbnail_two,'thumbnail-347-230')[0]; ?>" width="100%">
                                <p class="Valentine-special"><b><?php echo $video_two_title ?> |</b><i> <?php echo $video_two_subtitle ?></i></p>
                                <a data-video-link="<?php echo $video_two; ?>" href="#" class="icon-play-btn"><i class="fas fa-play"></i></a>
                            </div>
                        </button>
                        <button class="tablinks" onclick="openCity(event, 'Tokyo')">
                            <div class="special-img-btn">
                                <img src="<?php echo wp_get_attachment_image_src($video_thumbnail_three,'thumbnail-347-230')[0]; ?>" width="100%">
                                <p class="Valentine-special"><b><?php echo $video_three_title ?> |</b><i> <?php echo $video_three_subtitle ?></i></p>
                                <a data-video-link="<?php echo $video_three; ?>" href="#" class="icon-play-btn"><i class="fas fa-play"></i></a>
                            </div>
                        </button>
                    </div>
                    <div class="slide-btn2">
                        <a href="<?php echo $all_videos_link; ?>" class="btn-text">VIEW ALL videos</a>
                    </div>
                </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('feedback_section') ): ?>
        <?php while( have_rows('feedback_section') ): the_row();
            $title = get_sub_field('title');
            $image_one = get_sub_field('image_one');
            $date_one = get_sub_field('date_one');
            $text_one = get_sub_field('text_one');
            $image_two = get_sub_field('image_two');
            $date_two = get_sub_field('date_two');
            $text_two = get_sub_field('text_two');
            $image_three = get_sub_field('image_three');
            $date_three = get_sub_field('date_three');
            $text_three = get_sub_field('text_three');
            $image_four = get_sub_field('image_four');
            $date_four = get_sub_field('date_four');
            $text_four = get_sub_field('text_four');
            $off = get_sub_field('turn_off_section')[0]; ?>
            <?php if($off != 'off'): ?>
                <div class="feedback-sec">
                <div class="container">
                    <h1 class="feedback-heading"><?php echo $title ?></h1>

                    <div class="main-feedback">
                        <div class="category-img-sec">
                            <img src="<?php echo $image_one; ?>">
                        </div>
                        <div class="feedback-text">
                            <p><?php echo $date_one; ?><br><br><?php echo $text_one; ?></p>
                        </div>
                    </div>


                    <div class="main-feedback">
                        <div class="category-img-sec">
                            <img src="<?php echo $image_two; ?>">
                        </div>
                        <div class="feedback-text">
                            <p><?php echo $date_two; ?><br><br><?php echo $text_two; ?></p>
                        </div>
                    </div>

                    <div class="main-feedback">
                        <div class="category-img-sec">
                            <img src="<?php echo $image_three; ?>">
                        </div>
                        <div class="feedback-text">
                            <p><?php echo $date_three; ?><br><br><?php echo $text_three; ?></p>
                        </div>
                    </div>

                    <div class="main-feedback">
                        <div class="category-img-sec">
                            <img src="<?php echo $image_four; ?>">
                        </div>
                        <div class="feedback-text">
                            <p><?php echo $date_four; ?><br><br><?php echo $text_four; ?></p>
                        </div>
                    </div>

                    <div class="slide-btn2">
                        <a href="<?php echo get_permalink( get_page_by_path( 'testimonials' ) ) ?>" class="btn-text">VIEW ALL TESTIMONIALS</a>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php if( have_rows('see_on_section') ): ?>
        <?php while( have_rows('see_on_section') ): the_row();
            $title = get_sub_field('title');
            $button_text = get_sub_field('button_text');
            $button_link = get_sub_field('button_link');
            $image_one = get_sub_field('box_one_image');
            $text_one = get_sub_field('box_one_text');
            $image_two = get_sub_field('box_two_image');
            $text_two = get_sub_field('box_two_text');
            $image_three = get_sub_field('box_three_image');
            $text_three = get_sub_field('box_three_text');
            $off = get_sub_field('turn_off_section')[0]; ?>
            <?php if($off != 'off'): ?>
                <div class="seeon-sec">
                <div class="container">
                    <div class="eye-img">
                        <img src="<?php echo THEME_URL ?>/assets/images/41.png" >
                    </div>

                    <div>
                        <h1 class="seeon-heading"><?php echo $title; ?></h1>

                    </div>

                    <div class="main_seeon">
                        <div class="category-img-sec">
                            <img src="<?php echo $image_one ?>" border="1px solid #000">
                        </div>
                        <div class="seeon-text">
                            <h3><?php echo $text_one; ?></h3>
                        </div>
                    </div>


                    <div class="main_seeon">
                        <div class="category-img-sec">
                            <img src="<?php echo $image_two ?>" border="1px solid #000">
                        </div>
                        <div class="seeon-text">
                            <h3><?php echo $text_two; ?></h3>
                        </div>
                    </div>

                    <div class="main_seeon">
                        <div class="category-img-sec">
                            <img src="<?php echo $image_three ?>" border="1px solid #000">
                        </div>
                        <div class="seeon-text">
                            <h3><?php echo $text_three; ?></h3>
                        </div>
                    </div>

                    <div class="slide-btn2">
                        <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                    </div>


                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>

    <?php echo get_template_part('partials/subscribe') ?>

    <?php if( have_rows('instagram_section') ): ?>
        <?php while( have_rows('instagram_section') ): the_row();
            $instagram_title = get_sub_field('instagram_title');
            $instagram_url = get_sub_field('instagram_url');
            $instagram_link_text = get_sub_field('instagram_link_text');
            $images = get_sub_field('images_gallery');
            $size = 'thumbnail-369-369';
            $off = get_sub_field('turn_off_section')[0]; ?>
            <?php if($off != 'off'): ?>
                <div class="follow-sec">
                <div class="container">
                    <h1 class="feedback-heading"><?php echo $instagram_title ?> <a href="<?php echo $instagram_url; ?>" target="blank"><?php echo $instagram_link_text ?></a></h1>
                </div>
                <div class="follow-img-sec">
                    <div class="instagramFeedParent">
                        <div class=" cont_slide" >
                            <div class="slider center">
                                <?php if( $images ): ?>
                                        <?php foreach( $images as $image ): ?>
                                            <div class="clip">
                                                <div class="clipimg">
                                                    <?php echo wp_get_attachment_image($image, $size); ?>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        <?php endwhile; ?>
    <?php endif; ?>
</div>

<div class="popup-main">
    <div class="popup-inner">
        <div class="colse-icon">X</div>
        <div class="popup-content">
            <iframe id="youtbevideo" width="100%" height="100%" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>

<style>
    .clip {
        height: 100px;
        overflow: hidden;
        bottom: 0;
    }
    .center .slick-slide {
        height: 369px;
    }
    .center .slick-center img{
        opacity:1;
    }
    .center img{
        opacity:0.3;
        bottom: 0;
    }
    .clipimg{
        width: 369px;
        height: 369px;
    }
    .clipimg img{
        width: 100%;
        height: 100%;
    }
    .slick-slide{
        margin:0 5px;
    }
</style>
<script>
    $('.center').slick({
        dots: false,
        arrows: false,
        centerMode: true,
        autoplay: true,
        loop: true,
        speed: 1000,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 500,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.center').on('afterChange', function (event, slick, currentSlide) {
        $('.slick-slide').removeClass('slick-active-first slick-active-last');
        $('.slick-active').next().addClass('slick-center');
        $('.slick-active').next().next().addClass('slick-center');
        $('.slick-active').prev().addClass('slick-center');
    }).trigger('afterChange');
</script>
<?php
get_footer();

