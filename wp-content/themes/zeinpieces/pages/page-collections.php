<?php
/**
 * Template Name: Collections
 */

$terms_args = array(
    'taxonomy'   => 'collection',
    'hide_empty' => false,
);
$terms = get_terms( $terms_args );

$term_array = array();

//loop through all the terms
foreach ($terms as $term) {
    //set a variable for the custom field "sort" (the following assumes the use of ACF...if not, you'll need to use the more traditional way of getting a custom field)
    $sort_order = get_field( 'sort', $term );

    //push the object into the new array with its 'sort' as the key
    $term_array[$sort_order] = $term;
}

//sort the new array
ksort( $term_array, SORT_NUMERIC );

$collections_chunks = array_chunk( $term_array,2 );

get_header(); 
?>
    <div class="category-menu">
        <div class="container">
            <?php show_the_breadcrumbs(); ?>
        </div>
    </div>
    <div>
        <h1 class="seeon-heading"><?php echo get_the_title(); ?></h1>
    </div>
    <div class="container">
        <div class="collection-page">
            <?php foreach( $collections_chunks as $collections ) { ?>
                <?php foreach ( $collections as $collection ) {
                    $image = get_field('image', $collection);
                    ?>
                    <div class="main_collection">
                        <div class="category-img-sec">
                            <img src="<?php echo $image['sizes']['thumbnail-670-420']; ?>" alt="<?php echo $image['alt']; ?>">
                        </div>
                        <div class="collection-text">
                            <h3><?php echo $collection->name;?></h3>
                        </div>
                        <div class="btn-collection">
                            <a href="<?php echo get_term_link( $collection->term_id ); ?>">view now</a>
                        </div>
                    </div>
                <?php } ?>
            <?php }?>
        </div>
    </div>
    <div class="subscribe-sec-collection">
        <div class="container">
            <h3 class="subscribe-head">WE'D LOVE TO SHARE OUR NEW COLLECTIONS, LATEST NEWS AND MUCH MORE WITH YOU!</h3>
            <h1 class="main-heading-subscribe">SUBSCRIBE TO OUR NEWSLETTER</h1>
        </div>

        <div class="mail-sec container">
            <form action="<?php echo admin_url( 'admin-ajax.php' ); ?>" method="POST" id="newsletter-subscription-form">
                <div class="d-inline-block" style="margin-bottom: 14px;">
                    <input type="hidden" name="action" value="zp_newsletter">
                    <input type="hidden" name="zp_form_nonce" value="<?php echo wp_create_nonce('zp-nonce'); ?>"/>
                    <input type="email" name="email" class="emai-btn" placeholder="Email address" required>
                    <input type="submit" value="SUBMIT" class="submit-btn">
                </div>
                <div class="g-recaptcha" data-sitekey="6LeIpJAUAAAAAIuQY7Jb26POhNVNZ8y0MUdYDPcr"></div>
            </form>
        </div>
    </div>
<?php
get_footer();