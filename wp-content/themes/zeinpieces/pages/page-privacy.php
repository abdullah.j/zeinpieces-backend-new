<?php
/**
 * Template Name: Page Privacy Policy
 */

get_header(); ?>

    <div class="category-menu">
        <div class="container">
            <?php show_the_breadcrumbs();?>
        </div>
    </div>
    <div class="main">
        <div class="container">
            <div class="header-2-personalize">
                <h1><?php echo get_the_title(); ?></h1>
            </div>
        </div>

        <div class="sec-privacy-main">
            <div class="container">
                <div class="row-privacy">
                    <?php
                    if ( have_posts() ) : while ( have_posts() ) : the_post();
                        the_content();
                    endwhile; endif;
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php echo get_template_part('partials/subscribe') ?>

<?php get_footer();
