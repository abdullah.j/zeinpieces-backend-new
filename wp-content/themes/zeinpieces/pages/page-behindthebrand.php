<?php
/**
 * Template Name: Behind The Brand
 */

get_header(); ?>
    <div class="category-menu">
        <div class="container">
            <?php show_the_breadcrumbs();?>
        </div>
    </div>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="main">
            <div class="brand-heading">
                <div class="container">
                    <h2 class="kalimat-text"><?php the_title()?></h2>
                </div>
            </div>

            <div class="story-section">
                <div class="container">
                    <div class="main-brand-section">
                        <div class="left-img-story">
                            <div class="img-ls">
                                <?php the_post_thumbnail('thumbnail-487-680'); ?>
                            </div>
                        </div>
                        <div class="right-story">
                            <div class="st-text"><?php the_content(); ?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="zein-peices-sec">
                <div class="container">
                    <div class="zein-head">
                        <?php if( have_rows('section_two') ): ?>
                            <?php while( have_rows('section_two') ): the_row();
                                $title = get_sub_field('title');
                                $text = get_sub_field('text');
                                ?>
                                <h2 class="zein-head-question"><?php echo $title; ?></h2>
                            <?php echo $text ?>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="main-services-section">
                <div class="container">
                    <?php if( have_rows('section_three') ): ?>
                        <?php while( have_rows('section_three') ): the_row();
                            $main_title = get_sub_field('main_title');
                            ?>
                            <div class="service-heading">
                                <h2 class="zein-head-question"><?php echo $main_title; ?></h2>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    <?php if( have_rows('section_three') ): ?>
                        <?php while( have_rows('section_three') ): the_row();
                            $icon_one = get_sub_field('icon_one');
                            $title_one = get_sub_field('title_one');
                            $text_one = get_sub_field('text_one');
                            $icon_two = get_sub_field('icon_two');
                            $title_two = get_sub_field('title_two');
                            $text_two = get_sub_field('text_two');
                            $icon_three = get_sub_field('icon_three');
                            $title_three = get_sub_field('title_three');
                            $text_three = get_sub_field('text_three');
                            $icon_four = get_sub_field('icon_four');
                            $title_four = get_sub_field('title_four');
                            $text_four = get_sub_field('text_four');
                            $icon_five = get_sub_field('icon_five');
                            $title_five = get_sub_field('title_five');
                            $text_five = get_sub_field('text_five');
                            $icon_six = get_sub_field('icon_six');
                            $title_six = get_sub_field('title_six');
                            $text_six = get_sub_field('text_three');
                            ?>
                            <div class="gallery-service">
                                <div class="service-sec">
                                    <div class="service-main">
                                        <div class="img-services">
                                            <img src="<?php echo $icon_one; ?>" alt="Image">
                                        </div>
                                        <div class="servies-text">
                                            <h4><?php echo $title_one; ?></h4>
                                            <p><?php echo $text_one ?></p>
                                        </div>
                                    </div>
                                    <div class="service-main">
                                        <div class="img-services">
                                            <img src="<?php echo $icon_two; ?>" alt="Image">
                                        </div>
                                        <div class="servies-text">
                                            <h4><?php echo $title_two; ?></h4>
                                            <p><?php echo $text_two ?></p>
                                        </div>
                                    </div>
                                    <div class="service-main">
                                        <div class="img-services">
                                            <img src="<?php echo $icon_three; ?>" alt="Image">
                                        </div>
                                        <div class="servies-text">
                                            <h4><?php echo $title_three; ?></h4>
                                            <p><?php echo $text_three ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="service-sec">
                                    <div class="service-main">
                                        <div class="img-services">
                                            <img src="<?php echo $icon_four; ?>" alt="Image">
                                        </div>
                                        <div class="servies-text">
                                            <h4><?php echo $title_four; ?></h4>
                                            <p><?php echo $text_four ?></p>
                                        </div>
                                    </div>
                                    <div class="service-main">
                                        <div class="img-services">
                                            <img src="<?php echo $icon_five; ?>" alt="Image">
                                        </div>
                                        <div class="servies-text">
                                            <h4><?php echo $title_five; ?></h4>
                                            <p><?php echo $text_five ?></p>
                                        </div>
                                    </div>
                                    <div class="service-main">
                                        <div class="img-services">
                                            <img src="<?php echo $icon_six; ?>" alt="Image">
                                        </div>
                                        <div class="servies-text">
                                            <h4><?php echo $title_six; ?></h4>
                                            <p><?php echo $text_six ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>

            <div class="subscribe-sec-brand">
                <div class="container">
                    <h3 class="subscribe-head">WE'D LOVE TO SHARE OUR NEW COLLECTIONS, LATEST NEWS AND MUCH MORE WITH YOU!</h3>
                    <h1 class="main-heading-subscribe">SUBSCRIBE TO OUR NEWSLETTER</h1>
                </div>

                <div class="mail-sec container">
                    <form action="<?php echo admin_url( 'admin-ajax.php' ); ?>" method="POST" id="newsletter-subscription-form">
                        <div class="d-inline-block" style="margin-bottom: 14px;">
                            <input type="hidden" name="action" value="zp_newsletter">
                            <input type="hidden" name="zp_form_nonce" value="<?php echo wp_create_nonce('zp-nonce'); ?>"/>
                            <input type="email" name="email" class="emai-btn" placeholder="Email address" required>
                            <input type="submit" value="SUBMIT" class="submit-btn">
                        </div>
                        <div class="g-recaptcha" data-sitekey="6LeIpJAUAAAAAIuQY7Jb26POhNVNZ8y0MUdYDPcr"></div>
                    </form>
                </div>
            </div>
        </div>
    <?php endwhile; endif; ?>
<?php
get_footer();