<?php
/**
 * Template Name: Taxonomy gender template
 */

get_header();
$collection_args = array(
    'taxonomy'   => 'collection',
    'hide_empty' => false,
);
$collection_terms = get_terms( $collection_args );
$categories_args = array(
    'taxonomy'   => 'product_cat',
    'hide_empty' => false,
    'category__not_in' => array('15')
);
$categories_terms = get_terms( $categories_args );
?>
    <?php if( have_rows('section_one') ): ?>
        <?php while( have_rows('section_one') ): the_row();
            $image_url = get_sub_field('image');
            $title = get_sub_field('title');
            $text = get_sub_field('text');
            $button_text = get_sub_field('button_text');
            $button_link = get_sub_field('button_link'); ?>
            <div class="slide-sec">
                <div class="left-side">
                    <div class="yours-class">
                        <div class="slide-sec">
                            <div class="section-slide">
                                <div class="left-slide">
                                    <img src="<?php echo $image_url; ?>" width="100%" height="100%">
                                </div>
                                <div class="right-slide">
                                    <div class="slide-text">
                                        <h1 class="slide-heading"><?php echo $title; ?></h1>
                                        <p class="slide-sub-head"><?php echo $text; ?></p>
                                        <div class="slide-btn">
                                            <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; ?>
    <div class="main">
        <div>
            <h1 class="main-heding-category">shop by collection</h1>
        </div>
        <div class="multiple-items">
            <?php
            foreach($collection_terms as $collection_term){
                $image_id = get_field('image', $collection_term, false); // 3rd arg set to false to ensure we get unformatted value (ID)
                $image = wp_get_attachment_image_src($image_id, 'thumbnail-480-364');
                ?>
                <a href="<?php echo get_term_link($collection_term->term_id) ?>">
                    <div class="box">
                        <div class="img-s">
                            <img src="<?php echo $image[0]; ?>">
                        </div>
                        <div class="slide-text-head">
                            <h3><?php echo $collection_term->name; ?></h3>
                        </div>
                    </div>
                </a>
            <?php } ?>
        </div>
        <?php if( have_rows('section_two') ): ?>
            <?php while( have_rows('section_two') ): the_row();
                $image = wp_get_attachment_image_src( get_sub_field('image'), 'thumbnail-552-500' )[0];
                $title = get_sub_field('title');
                $text = get_sub_field('text');
                $button_text = get_sub_field('button_text');
                $button_link = get_sub_field('button_link');
                $products = get_sub_field('products');
                $off = get_sub_field('turn_off_section')[0];
                $reverse = get_sub_field('reverse_image_position')[0];
                $product_count = 0; ?>
                <?php if($off != 'off'): ?>
                    <div class="container">
                        <div class="name-sec">
                            <?php if($reverse == 'reverse'): ?>
                                <div class="right-name">
                                    <img src="<?php echo $image; ?>" width="100%" height="500px">
                                </div>
                                <div class="left-name">
                                    <div class="show-text">
                                        <h1 class="show-heading"><?php echo $title; ?></h1>
                                        <p class="show-sub-head"><?php echo $text; ?></p>
                                        <div class="slide-btn2">
                                            <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ?>
                            <?php if($reverse != 'reverse'): ?>
                                <div class="left-name">
                                    <div class="show-text">
                                        <h1 class="show-heading"><?php echo $title; ?></h1>
                                        <p class="show-sub-head"><?php echo $text; ?></p>
                                        <div class="slide-btn2">
                                            <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="right-name">
                                    <img src="<?php echo $image; ?>" width="100%" height="500px">
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="container">
                        <div class="category-sec">
                            <?php foreach($products as $product){
                                $product_count++;
                                if($product_count <= 4){ ?>
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <div class="main_cate1">
                                            <div class="category-img-sec">
                                                <img src="<?php echo get_the_post_thumbnail_url($product->ID,'thumbnail-255-260') ?>" alt="Image">
                                                <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id='. $product->ID .']') ?>
                                            </div>
                                            <div class="category-text">
                                                <h3><?php echo $product->post_title; ?></h3>
                                                <p><?php echo wc_get_product( $product->ID )->get_price(); echo str_repeat('&nbsp;', 1); echo get_woocommerce_currency() ?></p>
                                            </div>
                                        </div>
                                    </a>
                                    <?php
                                }
                            } ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if( have_rows('section_three_part_one') ): ?>
            <?php while( have_rows('section_three_part_one') ): the_row();
                $image = wp_get_attachment_image_src( get_sub_field('image'), 'thumbnail-552-500' )[0];
                $title = get_sub_field('title');
                $button_text = get_sub_field('button_text');
                $button_link = get_sub_field('button_link');
                $reverse = get_sub_field('reverse_image_position')[0];
                $off = get_sub_field('turn_off_section')[0]; ?>
                <?php if($off != 'off'): ?>
                    <div class="container">
                        <div class="name-sec">
                            <?php if($reverse != 'reverse'): ?>
                                <div class="right-name">
                                    <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                                </div>
                                <div class="left-name">
                                    <div class="show-text">
                                        <h1 class="show-heading"><?php echo $title; ?></h1>
                                        <div class="slide-btn2">
                                            <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if($reverse == 'reverse'): ?>
                                <div class="left-name">
                                    <div class="show-text">
                                        <h1 class="show-heading"><?php echo $title; ?></h1>
                                        <div class="slide-btn2">
                                            <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="right-name">
                                    <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if( have_rows('section_three_part_two') ): ?>
            <?php while( have_rows('section_three_part_two') ): the_row();
                $image =  wp_get_attachment_image_src( get_sub_field('image'), 'thumbnail-552-500' )[0];
                $title = get_sub_field('title');
                $button_text = get_sub_field('button_text');
                $button_link = get_sub_field('button_link');
                $reverse = get_sub_field('reverse_image_position')[0];
                $off = get_sub_field('turn_off_section')[0]; ?>
                <?php if($off != 'off'): ?>
                    <div class="container">
                        <div class="name-sec">
                            <?php if($reverse != 'reverse'): ?>
                                <div class="left-name">
                                    <div class="show-text">
                                        <h1 class="show-heading"><?php echo $title; ?></h1>
                                        <div class="slide-btn2">
                                            <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="right-name">
                                    <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                                </div>
                            <?php endif; ?>
                            <?php if($reverse == 'reverse'): ?>
                                <div class="right-name">
                                    <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                                </div>
                                <div class="left-name">
                                    <div class="show-text">
                                        <h1 class="show-heading"><?php echo $title; ?></h1>
                                        <div class="slide-btn2">
                                            <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if( have_rows('section_four') ): ?>
            <?php while( have_rows('section_four') ): the_row();
                $products = get_sub_field('products');
                $title = get_sub_field('title');
                $product_count = 0;
                ?>
                <div class="best-sec">
                    <div class="container">
                        <div class="category-sec">
                            <h1 class="recomended-sec"><?php echo $title ?></h1>
                            <?php foreach($products as $product){
                                $product_count++;
                                if($product_count <= 4){ ?>
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <div class="main_cate">
                                        <div class="category-img-sec">
                                            <img src="<?php echo get_the_post_thumbnail_url($product->ID,'thumbnail-155-260') ?>">
                                            <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id='. $product->ID .']') ?>
                                        </div>
                                        <div class="category-text">
                                            <h3><?php echo $product->post_title; ?></h3>
                                            <p><?php echo wc_get_product( $product->ID )->get_price(); echo str_repeat('&nbsp;', 1); echo get_woocommerce_currency() ?></p>
                                        </div>
                                    </div>
                                    </a>
                                <?php
                                }
                            } ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
        <div><h1 class="shop-by-category-sec">shop by category</h1></div>
        <div class="multiple-items-2">
            <?php
            foreach($categories_terms as $categories_term){
            $image_id = get_term_meta( $categories_term->term_id, 'thumbnail_id', true );
            $image = wp_get_attachment_url( $image_id );
            ?>
                <div class="box">
                    <div class="img-s1">
                        <img src="<?php echo $image; ?>">
                    </div>
                    <div class="slide2-text-head">
                        <h3><?php echo $categories_term->name; ?></h3>
                        <a href="<?php echo get_term_link($categories_term->term_id) ?>">View More</a>
                    </div>
                </div>
            <?php } ?>
        </div>
        <?php if( have_rows('section_five_part_one') ): ?>
            <?php while( have_rows('section_five_part_one') ): the_row();
                $image = wp_get_attachment_image_src( get_sub_field('image'), 'thumbnail-552-500' )[0];
                $title = get_sub_field('title');
                $button_text = get_sub_field('button_text');
                $button_link = get_sub_field('button_link');
                $reverse = get_sub_field('reverse_image_position')[0];
                $off = get_sub_field('turn_off_section')[0]; ?>
                <?php if($off != 'off'): ?>
                    <div class="container">
                        <div class="name-sec">
                            <?php if($reverse != 'reverse'): ?>
                                <div class="left-name">
                                    <div class="show-text">
                                        <h1 class="show-heading"><?php echo $title; ?></h1>
                                        <div class="slide-btn2">
                                            <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="right-name">
                                    <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                                </div>
                            <?php endif; ?>
                            <?php if($reverse == 'reverse'): ?>
                                <div class="right-name">
                                    <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                                </div>
                                <div class="left-name">
                                    <div class="show-text">
                                        <h1 class="show-heading"><?php echo $title; ?></h1>
                                        <div class="slide-btn2">
                                            <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if( have_rows('section_five_part_two') ): ?>
            <?php while( have_rows('section_five_part_two') ): the_row();
                $image = wp_get_attachment_image_src( get_sub_field('image'), 'thumbnail-552-500' )[0];
                $title = get_sub_field('title');
                $button_text = get_sub_field('button_text');
                $button_link = get_sub_field('button_link');
                $reverse = get_sub_field('reverse_image_position')[0];
                $off = get_sub_field('turn_off_section')[0];
                $products = get_sub_field('products');
                $product_count = 0;
                ?>
                <?php if($off != 'off'): ?>
                    <div class="container">
                        <div class="name-sec">
                            <?php if($reverse != 'reverse'): ?>
                                <div class="right-name">
                                    <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                                </div>
                                <div class="left-name">
                                    <div class="show-text">
                                        <h1 class="show-heading"><?php echo $title; ?></h1>
                                        <div class="slide-btn2">
                                            <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if($reverse == 'reverse'): ?>
                                <div class="left-name">
                                    <div class="show-text">
                                        <h1 class="show-heading"><?php echo $title; ?></h1>
                                        <div class="slide-btn2">
                                            <a href="<?php echo $button_link; ?>" class="btn-text"><?php echo $button_text; ?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="right-name">
                                    <img src="<?php echo $image; ?>" width="100%" height="500px" alt="Image">
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="container">
                        <div class="category-sec">
                            <?php foreach($products as $product){
                                $product_count++;
                                if($product_count <= 4){ ?>
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <div class="main_cate1">
                                                <div class="category-img-sec">
                                                    <img src="<?php echo get_the_post_thumbnail_url($product->ID,'thumbnail-255-260') ?>" alt="Image">
                                                    <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id='. $product->ID .']') ?>
                                                </div>
                                                <div class="category-text">
                                                    <h3><?php echo $product->post_title; ?></h3>
                                                    <p><?php echo wc_get_product( $product->ID )->get_price(); echo str_repeat('&nbsp;', 1); echo get_woocommerce_currency() ?></p>
                                                </div>
                                            </div>
                                    </a>
                                    <?php
                                }
                            } ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
        <?php if( have_rows('section_six') ): ?>
            <?php while( have_rows('section_six') ): the_row();
                $title = get_sub_field('title');
                $hash = get_sub_field('hash');
                $off = get_sub_field('turn_off_section')[0];
                ?>
                <?php if($off != 'off'): ?>
                    <div class="moments-sec">
                        <div class="container">
                            <h1 class="moments-head"></h1>
                            <h1 class="main-heading-moments">
                                <a target="_blank" href="https://www.instagram.com/zeinpieces/">#ZeinPiecesMoments</a>
                            </h1>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>

<?php get_footer();