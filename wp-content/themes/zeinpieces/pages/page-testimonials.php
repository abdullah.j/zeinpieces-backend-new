<?php
/**
 * Template Name: Testimonials
 */

get_header();
$counter = 0;
$all_counter = count(get_field('testimonials_section'));
?>
<div class="testimonial-page">
    <div class="category-menu">
        <div class="container">
            <?php show_the_breadcrumbs();?>
        </div>
    </div>
    <div class="main">
        <div class="testi-heading">
            <div class="container">
                <h2 class="kalimat-text"><?php the_content(); ?></h2>
            </div>
        </div>
        <div class="container">
            <div class="tab">
                <?php if( have_rows('videos_section') ): ?>
                    <?php while( have_rows('videos_section') ): the_row();
                        $video_one_title = get_sub_field('video_one_title');
                        $video_one_subtitle = get_sub_field('video_one_subtitle');
                        $video_one_link = get_sub_field('video_one_link', false);
                        $video_one_thumbnail = wp_get_attachment_image_src( get_sub_field('video_one_thumbnail'),'thumbnail-538-300' )[0];
                        $video_two_title = get_sub_field('video_two_title');
                        $video_two_subtitle = get_sub_field('video_two_subtitle');
                        $video_two_link = get_sub_field('video_two_link', false);
                        $video_two_thumbnail = wp_get_attachment_image_src( get_sub_field('video_two_thumbnail'),'thumbnail-538-300' )[0]; ?>
                        <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen">
                            <div class="special-img-btn">
                                <img src="<?php echo $video_one_thumbnail; ?>" width="100%">
                                <p class="Valentine-special"><?php echo $video_one_title; ?>  | <?php echo $video_one_subtitle; ?></p>
                                <a data-video-link="<?php echo $video_one_link; ?>" href="#" class="icon-play-btn"><i class="fas fa-play"></i></a>
                            </div>
                        </button>

                        <button class="tablinks" onclick="openCity(event, 'Paris')">
                            <div class="special-img-btn">
                                <img src="<?php echo $video_two_thumbnail; ?>" width="100%">
                                <p class="Valentine-special"><?php echo $video_two_title; ?>  | <?php echo $video_two_subtitle; ?></p>
                                <a data-video-link="<?php echo $video_two_link; ?>" href="#" class="icon-play-btn"><i class="fas fa-play"></i></a>
                            </div>
                        </button>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
        <?php if( have_rows('testimonials_section') ): ?>
            <?php while( have_rows('testimonials_section') ): the_row();
                $image = wp_get_attachment_image_src( get_sub_field('image'),'thumbnail-255-260' )[0];
                $date = get_sub_field('date');
                $text = get_sub_field('text');
                $counter = $counter +1;
                $all_counter = $all_counter -1;
                $initial_counter = ($counter == 1) ? "<div class='testimonial-feedback-sec'><div class='container'>" : '';
                $end_counter = ($counter <= 4) ? $end_value = ($all_counter == 0 || $counter == 4) ? '</div></div>' : '' : '';
                echo $initial_counter; ?>
                <div class="main-feedback">
                    <div class="category-img-sec">
                        <img src="<?php echo $image; ?>">
                    </div>
                    <div class="test-feedback-text">
                        <p><?php echo $date; ?><br><br>
                            <?php echo $text; ?>
                        </p>
                    </div>
                </div>
                <?php
                echo $end_counter;
                $reset_counter = ($counter == 4) ? $counter = 0 : ''; ?>
            <?php endwhile; ?>
        <?php endif; ?>
        <div class="testimonial-landed-sec">
            <div class="container">
                <div style="padding-top: 20px;"><h1 style="margin: 30px 0;" class="main-heding-category">JUST LANDED</h1></div>
                <div class="category-sec">
                    <?php if( have_rows('just_landed_section') ): ?>
                        <?php while( have_rows('just_landed_section') ): the_row();
                            $products = get_sub_field('just_landed_products');
                            $product_count = 0; ?>
                            <?php foreach($products as $product){
                                $product_count++;
                                if($product_count <= 4){ ?>
                                    <a href="<?php echo get_permalink( $product->ID ); ?>">
                                        <div class="main_cate">
                                            <div class="category-img-sec">
                                                <img src="<?php echo get_the_post_thumbnail_url($product->ID,'thumbnail-202-260') ?>" alt="Image">
                                                <?php echo do_shortcode('[ti_wishlists_addtowishlist product_id='. $product->ID .']') ?>
                                            </div>
                                            <div class="category-text">
                                                <h3><?php echo $product->post_title; ?></h3>
                                                <p><?php echo wc_get_product( $product->ID )->get_price(); echo str_repeat('&nbsp;', 1); echo get_woocommerce_currency() ?></p>
                                            </div>
                                        </div>
                                    </a>
                                    <?php
                                }
                            } ?>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="popup-main">
            <div class="popup-inner">
                <div class="colse-icon">X</div>
                <div class="popup-content">
                    <iframe id="youtbevideo" width="100%" height="100%" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
