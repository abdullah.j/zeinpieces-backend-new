<?php
/**
 * Template Name: Celebrities
 */

get_header();
$counter = 0;
$all_counter = count(get_field('celebrity_content'));
?>

    <div class="category-menu">
        <div class="container">
            <?php show_the_breadcrumbs();?>
        </div>
    </div>
    <div class="main">
        <div class="cel-seeon-heading">
            <div class="container">
                <h2 class="kalimat-text">SEEN ON</h2>
            </div>
        </div>
        <div class="main-must-gallery">
            <div class="container">
            <?php
            if( have_rows('celebrity_content') ):
                while( have_rows('celebrity_content') ) : the_row();
                    $counter = $counter +1;
                    $all_counter = $all_counter -1;
                    $initial_counter = ($counter == 1) ? "<div class='must-gallery-sec'>" : '';
                    $end_counter = ($counter <= 2) ? $end_value = ($all_counter == 0 || $counter == 2) ? '</div>' : '' : '';
                    $image = wp_get_attachment_image_src( get_sub_field('image'), 'thumbnail-551-415' )[0];
                    $name = get_sub_field('name');
                    echo $initial_counter; ?>
                        <div class="img-must-section">
                            <a href="#">
                                <img src="<?php echo $image ?>" class="celebrities-img">
                            </a>
                            <div class="celebrity-text">
                                <h4><?php echo $name ?></h4>
                            </div>
                        </div>
                    <?php
                    echo $end_counter;
                    $reset_counter = ($counter == 2) ? $counter = 0 : '';
                endwhile;
            endif;
            ?>
            </div>
        </div>
        <div class="subscribe-sec-brand">
            <div class="container">
                <h3 class="subscribe-head">WE'D LOVE TO SHARE OUR NEW COLLECTIONS, LATEST NEWS AND MUCH MORE WITH YOU!</h3>
                <h1 class="main-heading-subscribe">SUBSCRIBE TO OUR NEWSLETTER</h1>
            </div>
            <div class="mail-sec container">
                <form action="<?php echo admin_url( 'admin-ajax.php' ); ?>" method="POST" id="newsletter-subscription-form">
                    <div class="d-inline-block" style="margin-bottom: 14px;">
                        <input type="hidden" name="action" value="zp_newsletter">
                        <input type="hidden" name="zp_form_nonce" value="<?php echo wp_create_nonce('zp-nonce'); ?>"/>
                        <input type="email" name="email" class="emai-btn" placeholder="Email address" required>
                        <input type="submit" value="SUBMIT" class="submit-btn">
                    </div>
                    <div class="g-recaptcha" data-sitekey="6LeIpJAUAAAAAIuQY7Jb26POhNVNZ8y0MUdYDPcr"></div>
                </form>
            </div>
        </div>
    </div>
    <div class="slider-popup-main">
        <div class="slider-popup-inner">
            <div class="slider-colse-icon">X</div>
            <div class="slider-popup-content">
                <div class="pop-slide">
                    <div class="slider-for">
                        <?php
                        if( have_rows('celebrity_content') ):
                            while( have_rows('celebrity_content') ) : the_row();
                                $image = wp_get_attachment_image_src( get_sub_field('image'), 'thumbnail-700-500' )[0]; ?>
                                <div>
                                    <img src="<?php echo $image ?>" class="main-pop-slide">
                                </div>
                                <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                    <div class="slide-tab">
                        <?php
                        if( have_rows('celebrity_content') ):
                            while( have_rows('celebrity_content') ) : the_row();
                                $image = wp_get_attachment_image_src( get_sub_field('image'), 'thumbnail' )[0]; ?>
                                <div>
                                    <img src="<?php echo $image ?>" class="pop-celebrities-img">
                                </div>
                            <?php
                            endwhile;
                        endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
get_footer();
