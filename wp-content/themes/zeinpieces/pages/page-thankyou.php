<?php
/**
 * Template Name: Thank you
 */

get_header();
$backurl = "";
?>
    <div class="page-thankyou">
        <div class="category-menu">
            <div class="container">
                <?php show_the_breadcrumbs();?>
            </div>
        </div>

        <div class="container">
            <h1 class="inner-page-title">Thank you for contacting us</h1>
            <p>We have received your message, we will reach out to you the soonest. </p>
            <div class="thankyou-message">
                <p><a href="<?php echo site_url(); ?>">Click here</a> to go back to home page</p>
            </div>
        </div>
        <?php get_template_part('partials/you-may-also-like/from-our-collections'); ?>
    </div>
<?php
get_footer();
