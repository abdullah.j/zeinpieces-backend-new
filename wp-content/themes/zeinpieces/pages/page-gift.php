<?php
/**
 * Template Name: Gift Card
 */

get_header();
$page_id = get_the_ID();
$digital_text = get_field('digital_gift_text', $page_id);
$physical_text = get_field('physical_gift_text', $page_id);
$digital_image = wp_get_attachment_image_src( get_field('digital_gift_image'), 'large' )[0];
$physical_image = wp_get_attachment_image_src( get_field('physical_gift_image'), 'large' )[0];
?>

<div class="page">
    <div class="category-menu">
        <div class="container">
            <?php show_the_breadcrumbs(); ?>
        </div>

    </div>
    <div>
        <h1 class="seeon-heading"><?php echo get_the_title(); ?></h1>
    </div>
    <div class="container">
        <div class="name-sec">
            <div class="right-name">
                <img src="<?php echo $digital_image ?>" width="100%" height="500px">
            </div>
            <div class="left-name">
                <div class="show-text">
                    <h1 class="show-heading"><?php echo $digital_text ?></h1>
                    <div class="slide-btn2">
                        <a href="<?php echo get_permalink( get_page_by_title( 'Digital Gift', OBJECT, 'product' )->ID ) ?>" class="btn-text">View now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="name-sec image-right-sec gift-img mb-0">
            <div class="left-name">
                <div class="show-text">
                    <h1 class="show-heading"><?php echo $physical_text ?></h1>
                    <div class="slide-btn2">
                        <a href="<?php echo get_permalink( get_page_by_title( 'Physical Gift', OBJECT, 'product' )->ID ) ?>" class="btn-text">View Now</a>
                    </div>
                </div>
            </div>
            <div class="right-name">
                <img src="<?php echo $physical_image ?>" width="100%" height="500px">
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
