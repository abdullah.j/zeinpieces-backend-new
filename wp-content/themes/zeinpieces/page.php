<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package zeinpieces
 */

get_header(); ?>

    <div class="category-menu">
        <div class="container">
            <?php show_the_breadcrumbs();?>
        </div>
    </div>

    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <div class="main">
            <div class="container">
                <div class="header-2-personalize">
                    <h1><?php the_title()?></h1>
                </div>
            </div>
            <div class="sec-customer-service-main">
                <div class="container">
                    <div class="image"><?php the_post_thumbnail('thumbnail-670-420'); ?></div>
                    <div class="row-customer-service">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; endif; ?>

    <?php echo get_template_part('partials/subscribe') ?>

<?php
get_footer();
